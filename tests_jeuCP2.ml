(** {Fonctions test jeuCP2} *)

(* ****************** Leonard  ********************* *)
(** Fonction test draw absolute pt
     @author Leonard 
*)
let test_draw_absolute_pt_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"draw_absolute_pt_a") in
  let test_result : unit t_test_result = test_exec(test_step, draw_absolute_pt, (({x = 0 ; y = 0}), ({x = 20 ; y = 20}), 20, black)) in
    (
    assert_true(test_step, "succes 1", test_is_success(test_result));
    test_end(test_step)
    )
;;

(** Fonction test draw frame
     @author Leonard 
*)
let test_draw_frame_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"draw_frame_a") in
  let test_result : unit t_test_result = test_exec(test_step, draw_frame, (({x = 0; y = 0}), 340, 580, 20)) in
    (
    assert_true(test_step, "succes (({x = 0; y = 0}), 340, 580, 20)", test_is_success(test_result));
    test_end(test_step)
    )
;;

(* ****************** Xavier ********************* *)

(** Fonction test insert
     @author Xavier
     @param test matrice 'vide' (que du blanc)
*)
(* test valeur incorrect *)
let test_insert_a(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"insert_a")
  and param : t_param = init_param() in
  let arr_param_shape : t_shape t_array = param.shapes in
  let cur : t_cur_shape = cur_shape_choice(arr_param_shape, param.mat_szx-1, param.mat_szy-1, init_color()) in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x) in
  let pt_l : t_point list = shape.shape 
  and my_mat : t_color matrix= mat_make(param.mat_szx, param.mat_szy, white) in
  let test_result : bool t_test_result = test_exec(test_step, insert, (cur, pt_l, param, my_mat)) in
  (
    assert_true(test_step, "La pi�ce peut �tre ins�r�e", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test insert
     @author Xavier
     @param test matrice pleine (que du jaune)
*)
(* test valeur incorrect *)
let test_insert_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"insert_b")
  and param : t_param = init_param() in
  let arr_param_shape : t_shape t_array = param.shapes in
  let cur : t_cur_shape = cur_shape_choice(arr_param_shape, param.mat_szx-1, param.mat_szy-1, init_color()) in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x) in
  let pt_l : t_point list = shape.shape 
  and my_mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, yellow) in
  let test_result : bool t_test_result = test_fail_exec(test_step, insert, (cur, pt_l, param, my_mat)) in
  (
    assert_false(test_step, "La pi�ce ne doit pas �tre ins�r�e", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test valid_matrix_point
     @author Xavier
     @param test le premier point de la matrice
     @Test fonctionnel
 *)
let test_valid_matrix_point(status : t_test_status) : unit =
  let test_step = test_start(status, "test_valid_matrix_point")
  and point : t_point = {x = 0 ; y = 0}
  and param : t_param = init_param() in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "Le point [0,0] doit appartenir � la matrice", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step)
  )
;;

(** Fonction test valid_matrix_point_a
     @author Xavier
     @param test le dernier point de la matrice 
     @Test fonctionnel
*)
(* test valeur correct *)
let test_valid_matrix_point_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"valid_matrix_point_a")
  and param : t_param = init_param() in
  let point : t_point = {x = param.mat_szx-1 ; y = param.mat_szy-1} in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "Point qui doit appartenir � la matrice", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step)
    )
;;


(** Fonction test valid_matrix_point_b
     @author Xavier
     @param test un point hors matrice (au bord)
     @Test fonctionnel
*)
(* test valeur incorrect *)
let test_valid_matrix_point_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"valid_matrix_point_b")
  and param : t_param = init_param() in
  let point : t_point = {x = param.mat_szx ; y = param.mat_szy} in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "Point qui ne doit pas appartenir � la matrice", test_get(test_result), false)
    else test_error(test_step) ;
    test_end(test_step)
  )
;;

(** Fonction test is_free_move
     @author Xavier
     @param test une pi�ce au hasard dans une matrice vide (qui contient que du blanc)
*)
(* test correct *)
let test_is_free_move_a(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_free_move_a")
  and param : t_param = init_param() in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x)
  and mat: t_color matrix = mat_make(param.mat_szx, param.mat_szy, white) in
  let test_result : bool t_test_result = test_exec(test_step, is_free_move, (fst(shape.shape), shape.shape,
                                           mat, param)) in
  (
   assert_true(test_step, "La pi�ce peut bien bouger", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test is_free_move
     @author Xavier
     @param test une pi�ce au hasard dans une matrice qui remplie (contient que du jaune)
*)
(* test correct *)
let test_is_free_move_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_free_move_b")
  and param : t_param = init_param() in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, yellow)in
  let test_result : bool t_test_result = test_fail_exec(test_step, is_free_move, (fst(shape.shape), shape.shape,
                                           mat, param)) in
  (
    assert_false(test_step, "La pi�ce ne peut pas �tre ins�r�e", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test is_column_full
     @author Xavier
     @param test si une ligne al�toire est pleine dans une matrice 'vide' (qui contient que du blanc)
*)
let test_is_column_full_a(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_column_full_a")
  and param : t_param = init_param() in
  let size_x : int = param.mat_szx
  and y : int = rand_int(0, param.mat_szx -1)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, white)in
  let test_result : bool t_test_result = test_exec(test_step, is_column_full, (mat, y, size_x)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "La colonne est vide -> renvoie false", test_get(test_result), false)
    else test_error(test_step) ;
    test_end(test_step)
  )
;;

(** Fonction test is_column_full
     @author Xavier
     @param test si une ligne al�toire est pleine dans une matrice 'pleine' (qui contient que du jaune)
*)
let test_is_column_full_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_column_full_b")
  and param : t_param = init_param() in
  let size_x : int = param.mat_szx
  and y : int = rand_int(0, param.mat_szx -1)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, yellow)in
  let test_result : bool t_test_result = test_exec(test_step, is_column_full, (mat, y, size_x)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "La colonne doit �tre pleine -> renvoie true", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step) 
  )
;;

(** Fonction test is_column_full
     @author Xavier
     @param test si une ligne al�toire est pleine dans une matrice qui � la m�me ligne test�e est partiellement remplie (derni�re case de la ligne vide)
*)
(* test correct *)
let test_is_column_full_c(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_column_full_c")
  and param : t_param = init_param() in
  let size_x : int = param.mat_szx
  and y : int = rand_int(0, param.mat_szx -1)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, white)in
  for i=0 to size_x-2
  do
    mat.(y).(i) <- yellow
  done;
  let test_result : bool t_test_result = test_exec(test_step, is_column_full, (mat, y, size_x)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "La colonne n'est que partiellement 'pleine' -> renvoie false", test_get(test_result), false)
    else test_error(test_step) ;
    test_end(test_step) 
  )
;;

let test_run() : unit =
  let alltests : t_test_status = create_test_status() in
  (
    (* test de draw_absolut *)
    test_draw_absolute_pt_a(alltests);

    (* test de draw_frame *)
    test_draw_frame_a(alltests);

    (* test de insert *)
    test_insert_a(alltests);
    test_insert_b(alltests);

    (* test de valid_matrix_point *)
    test_valid_matrix_point(alltests);
    test_valid_matrix_point_a(alltests);
    test_valid_matrix_point_b(alltests);

    (* test de is_free_move *)
    test_is_free_move_a(alltests);
    test_is_free_move_b(alltests);

    (* test de is_column_full *)
    test_is_column_full_a(alltests);
    test_is_column_full_b(alltests);
    test_is_column_full_c(alltests);
    
    (* Print test status at the end *)
    print_test_report(alltests)
  )
;;

test_run();;


(* Test graphique - Appel simple de fonction *)

(* draw_absolut_pt -> trace le contour d'un carr� *)
draw_absolute_pt(({x = 14 ; y = 27}), ({x = 20 ; y = 20}), 20, black);;
draw_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, black);;
draw_absolute_pt(({x = 0 ; y = 1}), ({x = 50 ; y = 50}), 20, black);;

(* fill_absolute_pt -> trace un carr� (l'interieur) *)
fill_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
fill_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;

(* drawfill_absolute_pt -> trace un carr� complet *)
drawfill_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
drawfill_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
drawfill_absolute_pt(({x = 0 ; y = 1}), ({x = 50 ; y = 50}), 20, red);;

(* color_choice ->  choisit une couleur al�atoirement *)
color_choice(init_color());;

(* draw_relative_pt -> trace le contour d'un carr� *)
draw_relative_pt ( {x=0 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
draw_relative_pt ( {x=1 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
draw_relative_pt ( {x=2 ; y=9},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;

(* drawfill_relative_pt -> trace un carr� complet *)
drawfill_relative_pt ( {x=0 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=1 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=0 ; y=1},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=0 ; y=0},  {x=2 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=0 ; y=0},  {x=0 ; y=2} , {x=50 ; y=50} , 20 , red );;


clear_graph();;
