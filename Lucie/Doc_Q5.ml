(** {1 Documentation Question 5} *)

(*type ��a t_array = {len : int ; value : ��a array} ;;*)

(**Ce type permet de cr��er un tableaud'une longueur donn��e.

@param len longueur du tableau
@param value valeurs du tableau
*)

(*type t_shape = {shape : t_point list ; x_len : int ; y_len : int ;
rot_rgt_base : t_point ; rot_rgt_shape : int ;
rot_lft_base : t_point ; rot_lft_shape : int} ;;*)

(** Ce type permet de cr��er des formes qui seront g��n��r��s dans la matrice. Il prend en compte plusieurs donn��es comme l'amplitude en x et y de la forme et les formes r��sultant d'une rotation droit et gauche.

@param shape liste de points de coordoon��es des formes
@param x_len amplitude en x de la forme
@param y_len amplitude en y de la forme
@param rot_rgt_base r��sultat d'une rotation vers la droite
@param rot_rgt_shape r��sultat d'une rotation vers la droite
��param rot_lft_base r��sultat d'une rotation vers la gauche
@param rot_lft_shape r��sultat d'une rotation vers la gauche
*)

(*let init_sh011() : t_shape =
{shape = [{x = 0 ; y = 0} ; {x = 1 ; y = 0} ; {x = 2 ; y = 0} ; {x = 3 ; y = 0}] ;
x_len = 4 ; y_len = 1 ;
rot_rgt_base = {x = 1 ; y = 1} ; rot_rgt_shape = 1 ;
rot_lft_base = {x = 2 ; y = 1} ; rot_lft_shape = 1}
;;

let init_sh112() : t_shape =
{shape = [{x = 0 ; y = 0} ; {x = 0 ; y = -1} ; {x = 0 ; y = -2} ; {x = 0 ; y = -3}] ;
x_len = 1 ; y_len = 4 ;
rot_rgt_base = {x = -2 ; y = -1} ; rot_rgt_shape = 0 ;
rot_lft_base = {x = -1 ; y = -1} ; rot_lft_shape = 0}
;;
let init_sh211() : t_shape =
{shape = [{x = 0 ; y = 0} ; {x = 0 ; y = -1} ; {x = 1 ; y = 0} ; {x = 1 ; y = -1}] ;
x_len = 2 ; y_len = 2 ;
rot_rgt_base = {x = 0 ; y = 0} ; rot_rgt_shape = 2 ;
rot_lft_base = {x = 0 ; y = 0} ; rot_lft_shape = 2}
;;
*)

(** Ces fonctions definissent une forme concr��te pour pouvoir les appeler, utilisant le type pr��c��dent.

@param t_shape liste de points de coordoon��es des formes
@param x_len amplitude en x de la forme
@param y_len amplitude en y de la forme
@param rot_rgt_base r��sultat d'une rotation vers la droite
@param rot_rgt_shape r��sultat d'une rotation vers la droite
��param rot_lft_base r��sultat d'une rotation vers la gauche
@param rot_lft_shape r��sultat d'une rotation vers la gauche
*)

(*type t_cur_shape = {base : t_point ref ; shape : int ref ; color : t_color ref}
;;*)

(**Ce type permet la d��finition des formes concr��tes.

@param base coordoon��es du point d'origine de la forme
@param shape forme dans dans le tableau contenant le descriptif des formes
@param color couleur de la forme
*)

(*type t_param_time = {init : float ; extent : float ; ratio : float} ;;*)

(** Ce type contient les param��tres de temps, respectivement, la dur��e initiale d'un d��placement, la dur��e entre deux acc��l��rations et le coefficient d'acc��l��ration.

@param init dur��e initiale d'un d��placement exprim��e en secondes
@param extend dur��e entre deux acc��l��rations
@param ratio coefficient d'acc��l��ration
*)

(*type t_param_graphics =
  {base : t_point ; dilat : int ; color_arr : t_color t array} ;;*)

(**Ce type contient les param��tres graphiques, respectivement l'origine de l'espace d'affichage, le coefficient d'homoth��tie et le tableau de couleur des formes.

@param base origine de l'espace d'affichage
@param dilat coefficient d'homoth��tie entre l'espace de travail et l'espace d'affichage
@param color_arr tableau contenant les couleurs utilisables pour les formes concr��tes
*)

(*type t_param =
  {time : t_param_time ;
   mat_szx : int ; mat_szy : int ;
   graphics : t_param_graphics ;
   shapes : t_shape t_array
  }
;;
*)

(**Ce type contient les param��tres de temps ainsi que les param��tres graphiques, en plus des dimensions de la matrice et un tableau contenant les formes abstraites.

@param time param��tres de temps
@param mat_szx dimension de la matrice repr��sentant l'espace de travail en x
@param mat_szy dimension de la matrice repr��sentant l'espace de travail en y
@param graphics param��tres graphiques
@param shapes formes contenues dans le tableau de la fonction init_shapes
*)

(*let init_shapes() : t_shape t_array =
{len = 3 ; value = [| init_sh011() ; init_sh112() ; init_sh211() |]}
;;*)

(**Initialisation d'un tableau contenant les formes.*)

(*let init_color() : t_color t_array =
{len = 7 ; value = [|blue ; red ; green ; yellow ; cyan ; magenta ; grey|]}
;;*)

(**Initiaisation d'un tableau contenant les couleurs disponibles pour les formes concr��tes.*)

(*let init_param() : t_param =
{
time = {init = 1.0 ; extent = 10.0 ; ratio = 0.8} ;
mat_szx = 15 ; mat_szy = 28 ;
graphics = {base = {x = 50 ; y = 50} ; dilat = 20 ; color_arr = init_color()} ;
shapes = init_shapes()
}
;;*)

(**Initialisation d'une taille de matrice et des formes ainsi que les param��tres de temps et graphiques.*)

(*type t_play = {par : t_param ;
cur_shape : t_cur_shape ; mat : t_color matrix}
;;*)

(**Ce type permet de repr��senter les informations de jeu, contenant les param��tres de jeu, le descriptif de la forme actuelle qui se d��place et la matrice.*)
