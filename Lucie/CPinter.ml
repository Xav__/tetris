
(*
  When you launch the ocaml command: ocaml -init CPinter.ml
 *)

#load "graphics.cma";;
#load "unix.cma";;

#use "CPutil.ml";;
#use "jeuCP2.ml";;

open_graph(600, 900);;
draw_frame(({x=0; y = 0}), 340, 580, 20);;
clear_graph();;

  (*Question 1*)

let draw_absolute_pt(p, base_draw, dilat, col : t_point* t_point * int * t_color) : unit =
  set_color(col);
  let d : int = dilat - 1 in
  draw_rect(base_draw.x + (p.x * dilat), base_draw.y + (p.y * dilat), d, d)
;;

clear_graph();;
draw_absolute_pt(({x = 0 ; y = 0}), ({x = 20 ; y = 20}), 20, black);;
draw_absolute_pt(({x = 1 ; y = 0}), ({x = 20 ; y = 20}), 20, black);;
draw_absolute_pt(({x = 0 ; y = 1}), ({x = 20 ; y = 20}), 20, black);;

let fill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  set_color(col);
  let d : int = dilat - 3 in
  fill_rect(base_draw.x + (p.x * dilat)+1, base_draw.y + (p.y * dilat) +1, d, d)
;;
clear_graph();;
fill_absolute_pt(({x = 0 ; y = 0}), ({x = 20 ; y = 20}), 20, red);;
fill_absolute_pt(({x = 1 ; y = 0}), ({x = 20 ; y = 20}), 20, red);;


let drawfill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  draw_absolute_pt(p, base_draw, dilat, col);
  fill_absolute_pt(p, base_draw, dilat, col)
;;

drawfill_absolute_pt(({x = 0 ; y = 0}), ({x = 20 ; y = 20}), 20, red);;
drawfill_absolute_pt(({x = 1 ; y = 0}), ({x = 20 ; y = 20}), 20, red);;
drawfill_absolute_pt(({x = 0 ; y = 1}), ({x = 20 ; y = 20}), 20, red);;

  (*Question 2*)

let sum_point(p, base_point : t_point * t_point) : t_point =
      { x = p.x + base_point.x ; y = p.y + base_point.y}
  ;;

let draw_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
 draw_absolute_pt ( sum_point(p, base_point) , base_draw , dilat , col )
;;

draw_relative_pt(({x = 0 ; y = 0}), ({x = 1 ; y = 0}), ({x = 20 ; y = 40}), 20, red);;
draw_relative_pt(({x = 0 ; y = 0}), ({x = 2 ; y = 0}), ({x = 20 ; y = 40}), 20, red);;
draw_relative_pt(({x = 0 ; y = 0}), ({x = 3 ; y = 0}), ({x = 20 ; y = 40}), 20, red);;

let fill_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  fill_absolute_pt(sum_point(p, base_point), base_draw, dilat, col)
;;

fill_relative_pt(({x = 6 ; y = 15}), ({ x  = 1 ; y = 1}), ({x = 20 ; y = 20}), 20, red);;

let drawfill_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  drawfill_absolute_pt(sum_point(p, base_point), base_draw, dilat, col)
;;
drawfill_relative_pt(({x = 10 ; y = 5}), ({x = 1 ; y = 0}), ({x = 20 ; y = 40}), 20, red);;

  (*Question 4*)

type t_point = {x : int ; y : int} ;;
  
let draw_frame(base_draw, size_x, size_y, dilat : t_point * int * int * int) : unit =
  set_color(black);
   fill_rect(base_draw.x, base_draw.y, size_x - 1, dilat - 1);
   fill_rect(base_draw.x, base_draw.y, dilat - 1 , size_y);
   fill_rect(size_x - dilat, base_draw.y, dilat - 1 , size_y)
     ;;

     draw_frame(({x=0; y = 0}), 340, 580, 20);;


     

    
