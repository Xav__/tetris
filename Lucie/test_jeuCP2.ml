(** {1 Fonctions test jeuCP2} *)

let test_draw_absolute_pt_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"draw_absolute_pt_a") in
  let test_result : unit t_test_result = test_exec(test_step, draw_absolute_pt, (({x = 0 ; y = 0}), ({x = 20 ; y = 20}), 20, black)) in
    (
    assert_true(test_step, "succes 1", test_is_success(test_result));
    test_end(test_step)
    )
;;

(** 
Test de la fonction draw_absolute_pt.

@author L�onard
@param status liste qui contient les �tapes pr�c�dentes de tests pour d'autres fonctions
@param test_step modification de la liste status pour y ajouter un step
@param test_result r�sultat du test
@param test_exec test un focntionnement qui doit �tre correct
@param test_end met fin au test
@return "succes 1" si la fonction r�ussit � s'�x�cuter et  produit un r�sultat.
 *)

(*
let test_draw_absolute_pt_fail_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"draw_absolute_pt_fail_b") in
  let test_result : unit t_test_result = test_fail_exec(test_step, draw_absolute_pt, (({x = 0 ; y = 0}), ({x = 30 ; y = 23}), 19, black)) in
    (
      if test_is_success(test_result)
      then assert_equals(test_step, "fail (({x = 0 ; y = 0}), ({x = 30 ; y = 23}), 19, black)", test_fail_get(test_result), "error draw_abolute_pt : your parameter doesn't work")
      else test_error(test_step) ;
      test_end(test_step)
    )
;;
 *)

let test_draw_frame_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"draw_frame_a") in
  let test_result : unit t_test_result = test_exec(test_step, draw_frame, (({x = 0; y = 0}), 340, 580, 20)) in
    (
    assert_true(test_step, "succes (({x = 0; y = 0}), 340, 580, 20)", test_is_success(test_result));
    test_end(test_step)
    )
;;

(**
Test de la fonction draw_frame.

@author L�onard
@param status liste qui contient les �tapes pr�c�dentes de tests pour d'autres fonctions 
@param test_step modification de la liste status pour y ajouter un step
@param test_result r�sultat du test
@param test_exec test un fonctionnement qui doit �tre correct
@param test_end fin du test
@return "success ...." si la foncton r�ussit � s'�x�cuter et a produit un r�sultat.
 *)

let test_insert(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"insert") in
  let test_result : bool t_test_result = test_exec(test_step, insert, (cur_shape_choice(init_shapes(), 15, 27, init_color()), part_cube, init_param(), create_matrix(init_param()))) in
                                                   
    (
    assert_true(test_step, "succes", test_is_success(test_result));
    test_end(test_step)
    )
;;

(**
Test de la fonction insert.

@author Xavier
@param status liste qui contient les �tapes pr�c�dentes de tests pour d'autres fonctions 
@param test_step modification de la liste status pour y ajouter un step
@param test_result r�sultat du test
@param test_exec test un fonctionnement qui doit �tre correct
@param test_end fin du test
@return "success" si la foncton r�ussit � s'�x�cuter et a produit un r�sultat.
 *)

(*
let test_valid_matrix_point(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"valid_matrix_point") in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, ({x= 15; y= 24}, init_param())) in
                                                   
    (
    assert_true(test_step, "succes", test_is_success(test_result));
    test_end(test_step)
    )
;;*)

let test_valid_matrix_point(status : t_test_status) : unit =
  let test_step = test_start(status, "test_valid_matrix_point")
  and point : t_point = {x = 0 ; y = 0}
  and param : t_param = init_param() in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
  (
    assert_equals(test_step, "Point appartenant a la matrice", test_get(test_result), true);
    test_end(test_step)
  )
;;

(**
Test de la fonction valid_matrix_point.

@author Xavier
@param status liste qui contient les �tapes pr�c�dentes de tests pour d'autres fonctions
@param test_step modification de la liste status pour y ajouter un step
@param point coordonn�es dans l'espace de travail
@param param param�tres du jeu
@param test_result r�sultat du test
@param test_exec test un fonctionnement qui doit �tre correct
@param test_end fin du test
@return "Point appartenant a la matrice" si la foncton r�ussit � s'�x�cuter et a produit un r�sultat.
 *)






(** Fonction test valid_matrix_point
     @author Xavier
*)
(* test valeur correct *)
let test_valid_matrix_point_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"valid_matrix_point_a")
  and point : t_point = {x = 15 ; y = 24}
  and param : t_param = init_param() in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
    (
    if test_is_success(test_result)
    then assert_equals(test_step, "", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step)
    )
;;

(** Fonction test valid_matrix_point
     @author Xavier
*)
(* test valeur incorrect *)
let test_valid_matrix_point_b(status : t_test_status) : unit =
  let step : t_test_step = test_start(status,"valid_matrix_point_b")
  and point : t_point = {x = 15 ; y = 30}
  and param : t_param = init_param() in
  let result : bool t_test_result = test_fail_exec(step, valid_matrix_point, (point, param)) in
    (
    if test_is_success(result)
    then assert_equals(step,"fail y = 30", test_fail_get(result), "erreur valid_matrix_point : parametre invalide")
    else test_error(step) ;
    test_end(step)
    )
;;

(*
let test_draw_frame_fail_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status, "draw_frame_a") in
  let test_result : unit t_test_result = test_fail_exec(test_step, draw_frame, (({x = 0; y = 0}), 340, 580, 20)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "fail (({x = 0; y = 0}), 340, 580, 20)", test_fail_get(test_result), "error draw_frame : your parameter doesn't work")
    else test_error(test_step);
    test_end(test_step)
  )
*)

let test_run() : unit =
  let alltests : t_test_status = create_test_status() in
  (
    (* test de draw_absolut *)
    test_draw_absolute_pt_a(alltests);

    (* test de draw_frame *)
    test_draw_frame_a(alltests);

    (* test de insert *)
    test_insert(alltests);

    (* test de valid_matrix_point *)
    test_valid_matrix_point(alltests);
    test_valid_matrix_point_a(alltests);
    test_valid_matrix_point_b(alltests);

    
    (* Print test status at the end *)
    print_test_report(alltests)
  )
;;

test_run();;


(* Test graphique - Appel simple de fonction *)

(* draw_absolut_pt -> trace le contour d'un carr *)
draw_absolute_pt(({x = 14 ; y = 27}), ({x = 20 ; y = 20}), 20, black);;
draw_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, black);;
draw_absolute_pt(({x = 0 ; y = 1}), ({x = 50 ; y = 50}), 20, black);;

(* fill_absolute_pt -> trace un carr (l'interieur) *)
fill_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
fill_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;

(* drawfill_absolute_pt -> trace un carr complet *)
drawfill_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
drawfill_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
drawfill_absolute_pt(({x = 0 ; y = 1}), ({x = 50 ; y = 50}), 20, red);;

(* color_choice ->  choisit une couleur alatoirement *)
color_choice(init_color());;

(* draw_relative_pt -> trace le contour d'un carr *)
draw_relative_pt ( {x=0 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
draw_relative_pt ( {x=1 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
draw_relative_pt ( {x=2 ; y=9},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;

(* drawfill_relative_pt -> trace un carr complet *)
drawfill_relative_pt ( {x=0 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=1 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=0 ; y=1},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=0 ; y=0},  {x=2 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_relative_pt ( {x=0 ; y=0},  {x=0 ; y=2} , {x=50 ; y=50} , 20 , red );;

(* draw_pt_list ->  affiche une suite de contour de carr *)
draw_pt_list ( workspace (0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;

(* fill_pt_list ->  affiche une suite de carr rempli *)
fill_pt_list ( workspace (0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;

(* drawfill_pt_list ->  affiche une suite de carr complet *)
drawfill_pt_list ( workspace(0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;

(* test initilisation t_cur_shape *)
cur_shape_choice(init_shapes(), 15, 28, init_color());;


(* test fonction insert *)    (* -----------------------  faire ----------------------- *)
insert(cur_shape_choice(init_shapes(), 15, 28, init_color()), part_cube, init_param(), create_matrix(init_param()));;

clear_graph();;

