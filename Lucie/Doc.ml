open CPutil;;

(** {2 Documentation} *)

(** {3Question 1 :} *)

let draw_absolute_pt(p, base_draw, dilat, col : t_point* t_point * int * t_color) : unit =
  let d : int = dilat - 1 in
  (
  set_color(col);
  draw_rect(base_draw.x + (p.x * dilat), base_draw.y + (p.y * dilat), d, d)
  )
;;

(**
Cette fonction permet de tracer le contour d'un carré en fonction du point d'origine de l'espace d'affichage.

@author Xavier
@param p coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur du contour
@return le contour d'un carré aux coordonnées et de la couleur choisis.
 *)

let fill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  let d : int = dilat - 3 in
  (
  set_color(col);
  fill_rect(base_draw.x + (p.x * dilat)+1, base_draw.y + (p.y * dilat) +1, d, d)
  )
;;

(**
Cette fonction permet de remplir un carré sans contour en fonction du point d'origine de l'espace d'affichage.

@author Xavier
@param p coordonnées dans l'espace de travail
@param base_draw point d'origine de l'epace d'affichage
@param dilat homothétie de longueur du côté
@param col couleur de remplissage du carré
@return un carré plein aux coordonnées et de la couleur choisis.
 *)

let drawfill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  draw_absolute_pt(p, base_draw, dilat, col);
  fill_absolute_pt(p, base_draw, dilat, col)
;;

(**
Cette fonction permet de tracer le contour d'un carré d'une couleur puis de le remplir d'une couleur différente afin d'avoir un carré plein dont le contour est visible.

@author Xavier
@param p coordonnées dans l'espace de travail
@param base_draw point d'origine de l'epace d'affichage
@param dilat homothétie de longueur du côté
@param col couleur du contour et de remplissage du carré
@return un carré d'une couleur avec un contour d'une autre couleur aux coordonnées choisis.
 *)

(** {3 Question 2 :} *)

let sum_point(p, base_point : t_point * t_point) : t_point =
      { x = p.x + base_point.x ; y = p.y + base_point.y}
;;

(**
Cette fonction permet la translation du point p par le vecteur TB où T correspond à l'origine de l'espace de travail et B à base_point nécessaire pour les fonctions relative.

@author Lucie
@param p coordonnées dans l'espace de travail
@param base_point coordonnées dans l'espace de travail
 *)
let draw_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
 draw_absolute_pt ( sum_point(p, base_point) , base_draw , dilat , col )
;;

(**
Cette fonction permet de tracer le contour d'un carré en fonction du point d'origine de l'espace d'affichage où p est défini relativement au point base_point.

@author Lucie
@param p coordonnées dans l'espace de travail
@param base_point coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur du contour
@return le contour d'un carré aux coordonnées et de la couleur choisis.
 *)
let fill_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  fill_absolute_pt(sum_point(p, base_point), base_draw, dilat, col)
;;

(**
Cette fonction permet de remplir un carré sans contour en fonction du point d'origine de l'espace d'affichage où p est défini relativement au point base_point.

@author Lucie
@param p coordonnées dans l'espace de travail
@param base_point coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur de remplissage du carré
@return un carré plein aux coordonnées et de la couleur choisis.
 *)
let drawfill_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  drawfill_absolute_pt(sum_point(p, base_point), base_draw, dilat, col)
;;

(**
Cette fonction permet de tracer le contour d'un carré d'une couleur puis de le remplir d'une couleur différente afin d'avoir un carré plein dont le contour est visible où p est défini relativement au point base_point.

@author Lucie
@param p coordonnées dans l'espace de travail
@param base_point coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur du contour et de remplissage du carré
@return un carré d'une couleur avec un contour d'une autre couleur aux coordonnées choisis.
 *)


(** {3Question 3 :} *)

let rec draw_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , n : t_point list * t_point* t_point * int * t_color * int) : unit =
  if pt_list = []
  then(
    draw_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col);
    draw_pt_list_aux ( rem_fst ( pt_list ), base_pt, base_draw, dilat, col , n-1)
  )
  else
    draw_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col)
;;

let draw_pt_list( pt_list, base_pt, base_draw, dilat, col : t_point list * t_point* t_point * int * t_color) : unit =
  draw_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , len(pt_list))
;;

(**
Cette fonction permet de tracer plusieurs contours de carrés.

@author Théo
@param pt_list liste de points dans l'espace de travail
@param base_pt coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur des contours
@return les contours de plusieurs carrés aux coordonnées et de la couleur choisis.
 *)

let rec fill_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , n : t_point list * t_point* t_point * int * t_color * int) : unit =
  if n > 1
  then(
    fill_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col);
    fill_pt_list_aux ( rem_fst ( pt_list ), base_pt, base_draw, dilat, col , n-1)
  )
  else
    fill_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col)
;;

let fill_pt_list(pt_list, base_pt, base_draw, dilat, col : t_point list * t_point * t_point * int * t_color) : unit =
  fill_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , len(pt_list))
;;

(** 
Cette fonction permet de tracer plusieurs carrés plein.
 mlvp
@author Théo
@param pt_list liste de points dans l'espace de travail
@param base_pt coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur de remplissage des carrés
@return des carrés avec un remplissage de la couleur et aux ordonnées choisis.
 *)
let drawfill_pt_list(pt_list, base_pt, base_draw, dilat, col : t_point list * t_point * t_point * int * t_color) : unit =
  fill_pt_list(pt_list, base_pt, base_draw, dilat, col);
  draw_pt_list( pt_list, base_pt, base_draw, dilat, col)
;;

(**
Cette fonction permet de tracer plusieurs carrés et leurs contours de manière à ce que le contour puisse être différentié du carré remplis grâce aux couleurs.

@author Théo
@param pt_list liste de points dans l'espace de travail
@param base_pt coordonnées dans l'espace de travail
@param base_draw point d'origine de l'espace d'affichage
@param dilat homothétie de longeur du côté
@param col couleur des contours et de remplissage des carrés
@return des carrés d'une couleur avec des contours d'une autre couleur aux coordonnées choisis.
 *)

(** {3Question 4 :} *)

let draw_frame(base_draw, size_x, size_y, dilat : t_point * int * int * int) : unit =
  set_color(black);
  fill_rect(base_draw.x, base_draw.y, ((size_x + 3) * dilat) - 1, dilat - 1);
  fill_rect(base_draw.x, base_draw.y + dilat - 1, dilat - 1 , ((size_y + 1) * dilat) + 1);
  fill_rect(base_draw.x + ((size_x + 2) * dilat), base_draw.y + dilat - 1, dilat - 1, ((size_y + 1) * dilat) + 1);
  for i = 0 to size_x + 1
    do
      for j = 0 to size_y + 1
      do
        draw_absolute_pt({x = i; y=j}, base_draw, dilat, black)
      done;
    done
;;

(**
Cette fonction permet de tracer la partie de cadre entourant la zone d’affichageainsi que le cadrillaga de l'espace de travail.

@author Léonard, Lucie
@param base_draw point d'origine de l'espace d'affichage
@param size_x taille de la zone de travail en x
@param size_y taille de la zone de travail en y
@param dilat homothétie de longeur du côté
@return la partie de cadre entourant la zone d'affichage et son cadrillage.
 *)

(** {3 Question 7 :} *)

let color_choice(t : t_color t_array) : t_color =
  t.value.(rand_int(0, t.len -1)) 
;;

(**
Cette fonction permet de donner une couleur aléatoire parmis un tableau de 7 couleurs.

@author Xavier
@param t tableau contenant des couleurs
@return une couleur aléatoire prise dans un tableau qui les contient.
 *)

let point(cur, p : t_cur_shape * t_point) : t_point =
  let base : t_point ref  = cur.base in  
  let base_x : int = !base.x
  and base_y : int = !base.y in
  {x = base_x + p.x; y = base_y + p.y}
;;

(**
Cette fonction sert à cur_shape_choice.

@author Xavier
@param cur descriptif de la forme concrète
@param p coordonnées dans l'espace de travail
@return les coordonnées d'une pièce grâce à son point de base.
 *)

let cur_shape_choice(shapes, mat_szx, mat_szy, color_arr : t_shape t_array * int * int * t_color t_array) : t_cur_shape =
  let rand_shape : int = rand_int(0,6)
  and rand_color : t_color = color_choice(color_arr)
  and point : t_point = {x = mat_szx/2; y = mat_szy} in
  ({base = ref point; shape = ref (rand_shape); color = ref rand_color})
;;

(**
Cette fonction permet de générer aléatoirement un forme, sa couleur et sa position.

@author Xavier
@param shapes tabeau contenant des formes
@param mat_szx taille de la matrice en x
@param mat_szy taille de la matrice en y
@color_arr tableau contenant des couleurs
@return une forme aléatoire parmis celles stockés dans un tableau.
 *)

let rec insert_aux(cur, shape, my_mat : t_cur_shape * t_point list * t_color matrix) : bool =
  if(shape = [])
  then true
  else
    let p : t_point = point(cur, fst(shape)) in
    if(my_mat.(p.y).(p.x) = 1)
    then false
    else insert_aux(cur, rem_fst(shape), my_mat)
;;
  
let insert(cur, shape, param, my_mat : t_cur_shape * t_point list * t_param * t_color matrix) : bool =
  insert_aux(cur, shape, my_mat)
;;


(**
Cette fonction récursive permet de vérifier si la forme peut être insérée dans un espace avec d'autres formes sans qu'elle n'entre en collision avec ces dernières.

@author Xavier
@param cur descriptif de la forme concrète, couleur et point de base
@param shape coordonnées des points de base de la forme abstraite
@param param paramètres de jeu
@param my_mat espace de travail
@return true or false
 *)

let init_play () : t_play =
  {
    par= init_param() ;
    cur_shape = ( cur_shape_choice ( init_shapes() , param_mat_szx(init_param()) ,
                                     param_mat_szy(init_param()) , init_color()) );
    mat = mat_make ( param_mat_szx(init_param()) , param_mat_szy(init_param()) , 16777215 )
  }
;;

(**
Cette fonction permet d'intialiser les paramètres de jeu et l'espace de travail, choisit et insère la première forme concrète, trace le cadre de l'espace d'affichage, a pour résultat le descriptif de jeu ainsi défini.
 *)

(** {3 Question 8 :} *)

let valid_matrix_point ( p , param : t_point * t_param ) : bool =
 ( p.x >= 0 && p.x <= param_mat_szx(param)-1 )&& ( p.y >= 0 && p.y <= param_mat_szy(param)-1)
;;

(**
Cette fonction teste si un point est valide par rapport aux dimensions de l'espace de travail.

@author Théo
@param p coordonnées dans l'espace de travail
@param param paramètres du jeu
@return true or false
 *)

let rec is_free_move_aux ( p , shape , my_mat , param : t_point * t_point list * t_color matrix * t_param ) : bool =
  if shape = []
  then true
  else
    if valid_matrix_point ( fst(shape) , param ) = false
    then false
    else is_free_move_aux ( p , rem_fst(shape) , my_mat , param )
;;

let is_free_move ( p , shape , my_mat , param : t_point * t_point list * t_color matrix * t_param ) : bool =
  is_free_move_aux ( p , shape , my_mat , param )
;;

(**
Cette fonction teste si la forme décrite par le point base p et la liste de points shape d'une forme abstraite n'est pas en collision avec une forme insérée antérieurement.

@author Théo
@param p coordonnées dans l'espace de travail
@param shape coordonnées des points de base de la forme abstraite
@param my_mat espace de travail
@param param paramètres du jeu
@return true or false, si true alors pas de collision.
 *)
let move_left ( pl : t_play ) : unit =
  let a : t_point ref =  cur_shape_base(play_cur_shape(pl))
  and b : t_shape array = t_array_value (init_shapes())
  and c : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  if is_free_move ( {x= -1; y= 0} , shape_shape(b.(!c)) ,
                   play_mat(init_play()) , pl.par)
  then a:= {x= !a.x-1 ; y= !a.y }
  else failwith "erreur : rotation impossible"
;;

(**
Cette foncton effectue si possible, un déplacement de la forme courante d'une case à gauche.

@author Théo
@return le déplacement vers la gauche d'une forme dans l'espace de travail.
 *)

let move_right ( pl : t_play ) : unit =
   let a : t_point ref =  cur_shape_base(play_cur_shape(pl))
  and b : t_shape array = t_array_value (init_shapes())
  and c : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  if is_free_move ( {x= -1; y= 0} , shape_shape(b.(!c)) ,
                   play_mat(init_play()) , pl.par)
  then a:= {x= !a.x+1 ; y= !a.y }
  else failwith "erreur : rotation impossible"
;;

(**
Cette foncton effectue si possible, un déplacement de la forme courante d'une case à droite.

@author Théo
@return le déplacement vers la droite d'une forme dans l'espace de travail.
 *)

let move_down ( pl : t_play ) : bool =
  let a : t_point ref =  cur_shape_base(play_cur_shape(pl))
  and b : t_shape array = t_array_value (init_shapes())
  and c : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  if is_free_move ( {x= -1; y= 0} , shape_shape(b.(!c)) ,
                    play_mat(init_play()) , pl.par)
  then (
    a:= {x= !a.x ; y= !a.y-1 };
    true
       )
  else false
;;

(**
Cette foncton effectue si possible, un déplacement de la forme courante d'une case vers le bas.

@author Théo
@return true or false, si true, effectue déplacement.
 *)

let rotate_left ( pl : t_play ) : unit =
  let a : t_shape array = t_array_value (init_shapes())
  and b : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  drawfill_pt_list (shape_shape(a.(shape_lft_shape_rot (a.(!b)))) ,
                    shape_lft_base_rot(a.(!b)) ,{x = 50 ; y = 50}  , 20 , red)
;;

(** 
Cette fonction effectue si possible, une rotation de la forme vers la gauche.

@author Théo
@return la rotation de la forme vers la gauche.
 *)

let rotate_right ( pl : t_play ) : unit =
   let a : t_shape array = t_array_value (init_shapes())
  and b : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  drawfill_pt_list (shape_shape(a.(shape_rgt_shape_rot (a.(!b)))) ,
                    shape_rgt_base_rot(a.(!b)) ,{x = 50 ; y = 50}  , 20 , red)
;;

(** 
Cette fonction effectue si possible, une rotation de la forme vers la droite.

@author Théo
@return la rotation de la forme vers la droite.
 *)

let move_to_bottom ( pl : t_play ) : unit =
  while move_down ( pl ) = true
  do
    move_down2 ( pl );
  done
;;

(**
Cette fonction effectue un déplacement verticale de la forme le plus bas possible dans l'espace de travail, jusqu'au cadre ou jusqu'à se faire bloquer par une forme figée antérieurement.

@author Théo
@return déplacement vertical le plus bas possible dans les limites des fonctions précédentes.
 *)

(** {3 Question 9 :} *)

let is_column_full ( my_mat , y , size_x : t_color matrix * int * int ) : bool =
  let is_full : bool ref = ref false
  and szx : int = size_x - 1
  and line_full : int ref = ref 0 in
  let a : int ref = ref szx in
  for i = 0 to szx
  do
    if my_mat.(y).(i) <> 16777215
    then(
      line_full := !line_full + 1;
      if !line_full = 15
      then
        is_full := true
      else
        is_full := !is_full
    )
  done ;
  !is_full
;;

(**
Cette fonction teste si une colonne de la matrice est pleine.

@author Théo
@param my_mat espace de travail
@param y ordonnée des points de la colonne
@param syze_x longueur d'une ligne de la matrice
@return true or false, si true alors la colonne est pleine
 *)

let decal ( my_mat , y , size_x , size_y , param : t_color matrix * int * int * int * t_param ) : t_color array array (*unit*) =
  let a : int = size_x - 1
  and b :int = size_y - 2 in
    for i = y to b
    do
      for j = 0 to a
      do
      my_mat.(j).(i) <- my_mat.(j).(i+1)
      done;
    done;
  my_mat
;;
(**
Cette fonction décale vers le bas les colonnes de la matrice.

@author
@param my_mat espace de travail
@param y ordonnées des points de la colonne
@param size_x dimension de la matrice my_mat en x 
@param size_y dimension de la matrice my_mat en y
@param param paramètres du jeu
@return le décalage des colonnes de la matrice vers le bas.
 *)

let clear_play ( pl : t_play ) : unit =
  let a : bool ref = ref false in
  for i = 0 to ( param_mat_szy(play_par(pl)) - 1 )
  do
    a:= is_column_full ( play_mat(pl) , i , param_mat_szx(play_par(pl)) );
    while !a = true
    do
      for j = 0 to ( param_mat_szx(play_par(pl)) - 1 )
      do
        (play_mat(pl)).(j).(i) <- 16777215
      done;
      a:= false
    done;
  done
;;

(**
Cette fonction nettoie l'espace de travail, suppriment toutes les lignes pleines.

@author Théo
@return la suppression des lignes pleines.
 *)

let final_insert ( cur , shape , my_mat : t_cur_shape * t_point list * t_color matrix ) : unit =
  let a : t_point ref =  cur_shape_base(cur) in
  while is_free_move ( {x = !a.x ; y = !a.y} , shape , my_mat , init_param() ) = false
  do
    my_mat.( !a.x ).( !a.y ) <- color_choice ( init_color() )
  done;
;;


(**
Cette fonction permet de figer la forme courante pour qu'elle soit insérée dans la matrice.

@author Théo
@param cur forme concrète aléatoirement choisis dans un tableau
@param shape forme abstraite 
@param my_mat espace de travail
@return l'insertion de la forme courante dans la matrice.
 *)

let final_newstep(pl : t_play) : bool =
  let shape : t_point list = shape_shape( (t_array_value(init_shapes())).(!(cur_shape_shape(play_cur_shape(init_play())))) )
  and p : t_point ref = cur_shape_base(play_cur_shape(pl)) in
    if is_free_move(!p, shape, play_mat(init_play()), init_param())
    then ()
    else final_insert(play_cur_shape(pl), shape, play_mat(init_play()));
    
    if is_column_full( play_mat(init_play()), !p.y , param_mat_szx(play_par(pl))-1 )
    then(
      clear_play(pl);
      decal( play_mat(pl) , !p.y , param_mat_szx(play_par(pl))-1 , param_mat_szy(play_par(pl))-1 , play_par(pl));
      true )
    else false
;;

(**
Cette fonction teste si la forme courante peut encore se déplacer.

@author Théo
@return true or false, si true le jeu continue, si false le jeu s'arrête.
 *)

