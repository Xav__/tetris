#use "CPinter.ml" ;;

(****** Opengraph *******)

open_graph ( 800 , 800 ) ;;

(* -------------------------- *)
(* -------------------------- *)
(*    fonctions utilitaires   *)
(* -------------------------- *)
(* -------------------------- *)


let mywait(x : float) : unit =
  let y : float ref = ref (Sys.time()) in
  while (Sys.time() -. !y) < x
  do ()
  done
;;


(* --------------------------------- *)
(* --------------------------------- *)
(*   Types et fonctions graphique    *)
(* --------------------------------- *)
(* --------------------------------- *)

type t_point = {x : int ; y : int} ;;


(* ------------------------------------------------- *)
(* ------------------------------------------------- *)
(*    Types, formes, parametrage et initialisation   *)
(* ------------------------------------------------- *)
(* ------------------------------------------------- *)


(* Types *)

type 'a t_array = {len : int ; value : 'a array} ;;

type t_shape = {shape : t_point list ; x_len : int ; y_len : int ; 
                rot_rgt_base : t_point ; rot_rgt_shape : int ; 
                rot_lft_base : t_point ; rot_lft_shape : int} ;; 

type t_cur_shape = {base : t_point ref ; shape : int ref ; color : t_color ref} ;;


type t_param_time = {init : float ; extent : float ; ratio : float} ;;

type t_param_graphics = 
    {base : t_point ; dilat : int ; color_arr : t_color t_array} ;;

type t_param = 
  {time : t_param_time ; 
   mat_szx : int ; mat_szy : int ;
   graphics : t_param_graphics ; 
   shapes : t_shape t_array
} ;;

type t_play = {par : t_param ; cur_shape : t_cur_shape ; mat : t_color matrix} ;;

(* ****************** theo *************************** *)
(* ************ extraction de parametres ************* *)

let t_array_len ( g : 'a t_array ) : int =
  g.len
;;

let t_array_value ( g : 'a t_array ) : 'a array =
  g.value
;;

let time_init (h : t_param ) : float =
  h.time.init
;;


let time_extent (h : t_param ) : float =
  h.time.extent
;;

let time_ratio (h : t_param ) : float =
  h.time.ratio
;;

let shape_shape ( a : t_shape ) : t_point list =
  a.shape
;;

let shape_xlen ( a : t_shape ) : int =
  a.x_len
;;

let shape_ylen ( a : t_shape ) : int =
  a.y_len
;;

let shape_rgt_base_rot ( a : t_shape ) : t_point =
  a.rot_rgt_base
;;

let shape_lft_base_rot ( a : t_shape ) : t_point =
  a.rot_lft_base
;;

let shape_rgt_shape_rot ( a : t_shape ) : int =
  a.rot_rgt_shape
;;

let shape_lft_shape_rot( a : t_shape ) : int =
  a.rot_rgt_shape
;;

let cur_shape_base( b : t_cur_shape ) : t_point ref  =
  b.base
;;

let cur_shape_shape( b : t_cur_shape ) : int ref =
  b.shape
;;

let cur_shape_color( b : t_cur_shape ) : t_color ref =
  b.color
;;

let param_time_init ( c : t_param_time ) : float =
  c.init
;;

let param_time_extent ( c : t_param_time ) : float =
  c.extent
;;

let param_time_ratio ( c : t_param_time ) : float =
  c.ratio
;;

let param_graphics_base ( d : t_param_graphics ) : t_point =
  d.base
;;

let param_graphics_dilat ( d : t_param_graphics ) : int =
  d.dilat
;;

let param_time ( e : t_param ) : t_param_time =
  e.time
;;

let param_mat_szx ( e : t_param ) : int =
  e.mat_szx
;;
let param_mat_szy ( e : t_param ) : int =
  e.mat_szy
;;

let param_graphics ( e : t_param ) : t_param_graphics =
  e.graphics
;;

let param_shapes ( e : t_param ) : t_shape t_array  =
  e.shapes
;;

let play_par ( f : t_play ) : t_param =
  f.par
;;

let play_cur_shape ( f : t_play ) : t_cur_shape =
  f.cur_shape
;;

let play_mat ( f : t_play ) : t_color matrix =
  f.mat
;;

(* ***************** Xavier *********************** *)
(* Trace le rectangle d'un carr� *)

let draw_absolute_pt(p, base_draw, dilat, col : t_point* t_point * int * t_color) : unit =
  set_color(col);
  let d : int = dilat - 1 in
  draw_rect(base_draw.x + (p.x * dilat), base_draw.y + (p.y * dilat), d, d)
;;

(* ***************** Xavier *********************** *)
(* Remplir un rectangle *)

let fill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  set_color(col);
  let d : int = dilat - 3 in
  fill_rect(base_draw.x + (p.x * dilat)+1, base_draw.y + (p.y * dilat) +1, d, d)
;;

(* ***************** Xavier *********************** *)
(* tracer un rectangle et le remplir *)
let drawfill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  draw_absolute_pt(p, base_draw, dilat, black);
  fill_absolute_pt(p, base_draw, dilat, red)
;;

(* ***************** Xavier *********************** *)

let draw_relative_pt (p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color ) : unit =
  let d : int = dilat-1 in
  set_color ( black );
  draw_rect( base_draw.x + ( (base_point.x + p.x) * dilat ) , base_draw.y + ( (base_point.y + p.y) * dilat )  , d , d )
;;

(* ******************* Theo ********************** *)

let fill_relative_pt (p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color ) : unit =
  let d : int = dilat-1 in
  set_color ( col );
  fill_rect( base_draw.x + ( (base_point.x + p.x) * dilat ) , base_draw.y + ( (base_point.y + p.y) * dilat )  , d , d )
;;

(* ******************* Theo ********************** *)

let drawfill_relative_pt (p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  fill_relative_pt ( p, base_point , base_draw , dilat , col );
  draw_relative_pt ( p, base_point , base_draw , dilat , col )
;;

(* ******************* Theo ********************** *)

let rec draw_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , n : t_point list * t_point* t_point * int * t_color * int ) : unit =
  if n > 1
  then(
    draw_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col);
    draw_pt_list_aux ( rem_fst ( pt_list ), base_pt, base_draw, dilat, col , n-1 )
  )
  else
    draw_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col)
;;

let draw_pt_list( pt_list, base_pt, base_draw, dilat, col : t_point list * t_point* t_point * int * t_color) : unit =
  draw_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col  , len(pt_list))
;;

(* ******************* Theo ********************** *)

let rec fill_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , n : t_point list * t_point* t_point * int * t_color * int) : unit =
  if n > 1
  then(
    fill_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col);
    fill_pt_list_aux ( rem_fst ( pt_list ), base_pt, base_draw, dilat, col , n-1)
  )
  else
    fill_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col)
;;

let fill_pt_list( pt_list, base_pt, base_draw, dilat, col : t_point list * t_point* t_point * int * t_color) : unit =
  fill_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , len(pt_list))
;; 

(* ******************* Theo ********************** *)

let drawfill_pt_list(pt_list, base_pt, base_draw, dilat, col : t_point list * t_point * t_point * int * t_color) : unit =
  fill_pt_list(pt_list, base_pt, base_draw, dilat, col);
  draw_pt_list( pt_list, base_pt, base_draw, dilat, col)
;;

(* Initialisation des formes et des parametres *)

(* La barre *)

let part_barre_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=3 ; y=0 }];;
let part_barre_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y= -1 } ; {x=0 ; y= -2 } ; {x=0 ; y= -3 }];;

(* Le cube *)

let part_cube : t_point list = [{x=0 ; y=0 } ; {x= -1 ; y=0 } ; {x=0 ; y= -1 } ; {x= -1 ; y= -1 }];;

(* Le 'L' *)

let part_L_1 : t_point list = [{x=0 ; y=0 } ; {x= -1 ; y=0 } ; {x=0 ; y= -1 } ; {x=0 ; y= -2 }];;

let part_L_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y= -1 } ; {x= -1 ; y= -1 } ; {x= -2 ; y= -1 }];;
let part_L_3 : t_point list = [{x= -1 ; y=0 } ; {x= -1 ; y= -1 } ; {x= -1 ; y= -2 } ; {x=0 ; y= -2 }];;
let part_L_4 : t_point list = [{x=0 ; y=0 } ; {x= -1 ; y=0 } ; {x= -2 ; y=0 } ; {x= -2 ; y= -1 }];;

(* Le 'J' *)

let part_J_1 : t_point list = [{x=0 ; y=0 } ; {x= -1 ; y=0 } ; {x= -1 ; y= -1 } ; {x= -1 ; y= -2 }];;
let part_J_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y= -1 } ; {x= -1 ; y=0 } ; {x= -2 ; y=0 }];;
let part_J_3 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y= -1 } ; {x=0 ; y= -2 } ; {x= -1 ; y= -2 }];;
let part_J_4 : t_point list = [{x=0 ; y= -1 } ; {x= -1 ; y= -1 } ; {x= -2 ; y= -1 } ; {x= -2 ; y=0 }];;

(* Le S-Tetrimino *)

let part_S_1 : t_point list = [{x=0 ; y=0 } ; {x= -1 ; y=0 } ; {x= -1 ; y= -1 } ; {x= -2 ; y= -1 }];;
let part_S_2 : t_point list = [{x=0 ; y= -1 } ; {x=0 ; y= -2 } ; {x= -1 ; y=0 } ; {x= -1 ; y= -1 }];;

(* Le Z-Tetrimino *) 

let part_Z_1 : t_point list = [{x=0 ; y= -1 } ; {x= -1 ; y= -1 } ; {x= -1 ; y=0 } ; {x= -2 ; y=0 }];;
let part_Z_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y= -1 } ; {x= -1 ; y= -1 } ; {x= -1 ; y= -2 }];;

(* le T-Tetrimino *) 

let part_T_1 : t_point list = [{x=0 ; y= -1 } ; {x= -1 ; y= -1 } ; {x= -1 ; y=0 } ; {x= -2 ; y= -1 }];;
let part_T_2 : t_point list = [{x=0 ; y= -1 } ; {x= -1 ; y=0 } ; {x= -1 ; y= -1 } ; {x= -1 ; y= -2 }];;
let part_T_3 : t_point list = [{x=0 ; y=0 } ; {x= -1 ; y=0 } ; {x= -1 ; y= -1 } ; {x= -2 ; y=0 }];;
let part_T_4 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y= -1 } ; {x=0 ; y= -2 } ; {x= -1 ; y= -1 }];;

let init_barre_1() : t_shape = 
  {shape = part_barre_1 ; 
  x_len = 4 ; y_len = 1 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 1 ; 
  rot_lft_base = {x = 10 ; y = 10} ; rot_lft_shape = 1} 
;;

let init_barre_2() : t_shape = 
  {shape = part_barre_2 ; 
  x_len = 1 ; y_len = 4 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 0 ; 
  rot_lft_base = {x = 10 ; y = 10} ; rot_lft_shape = 0} 
;;

let init_cube() : t_shape = 
  {shape = part_cube ; 
  x_len = 2 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 2 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 2} 
;;

let init_L_1() : t_shape = 
  {shape = part_L_1 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 4 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 6} 
;;

let init_L_2() : t_shape = 
  {shape = part_L_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 5 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 3} 
;;
let init_L_3() : t_shape = 
  {shape = part_L_3 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 6 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 4} 
;;

let init_L_4() : t_shape = 
  {shape = part_L_4 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 3 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 5} 
;;

let init_J_1() : t_shape = 
  {shape = part_J_1 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 8 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 10} 
;;

let init_J_2() : t_shape = 
  {shape = part_J_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 9 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 7} 
;;

let init_J_3() : t_shape = 
  {shape = part_J_3 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 10 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 8} 
;;

let init_J_4() : t_shape = 
  {shape = part_J_4 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 7 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 9} 
;;

let init_S_1() : t_shape = 
  {shape = part_S_1 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 12 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 12} 
;;

let init_S_2() : t_shape = 
  {shape = part_S_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 11 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 11} 
;;

let init_Z_1() : t_shape = 
  {shape = part_Z_1 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 14 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 14} 
;;

let init_Z_2() : t_shape = 
  {shape = part_Z_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 13 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 13} 
;;

let init_T_1() : t_shape = 
  {shape = part_T_1 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 16 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 18} 
;;

let init_T_2() : t_shape = 
  {shape = part_T_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 17 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 15} 
;;

let init_T_3() : t_shape = 
  {shape = part_T_3 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 18 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 16} 
;;

let init_T_4() : t_shape = 
  {shape = part_T_4 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 10 ;  y = 10} ; rot_rgt_shape = 15 ; 
  rot_lft_base = {x = 10 ;  y = 10} ; rot_lft_shape = 17} 
;;

let init_shapes() : t_shape t_array = 
  {len = 18 ; value = [| init_barre_1() ; init_barre_2() ; init_cube() ; init_L_1();
                         (*init_L_2() ;*)init_L_3 (); init_L_4() ; init_J_1() ; init_J_2() ;
                         init_J_3() ; init_J_4() ;init_S_1() ; init_S_2() ; init_Z_1() ;
                         init_Z_2() ; init_T_1() ; init_T_2() ;init_T_3() ; init_T_4() |]} 
;;

let init_color() : t_color t_array = 
  {len = 7 ; value = [|blue ; red ; green ; yellow ; cyan ; magenta ; grey|]}
;;

let init_param() : t_param = 
    {
    time = {init = 1.0 ; extent = 10.0 ; ratio = 0.8} ; 
    mat_szx = 15 ; mat_szy = 28 ;
    graphics = {base = {x = 50 ; y = 50} ; dilat = 20 ; color_arr = init_color()} ; 
    shapes = init_shapes()
    }
;;

(* ****************** Leonard & Lucie ********************* *)

let draw_frame(base_draw, size_x, size_y, dilat : t_point * int * int * int) : unit =
set_color(black);
fill_rect(base_draw.x, base_draw.y, ( ( size_x + 2 ) * dilat ) - 1 , dilat - 1);
fill_rect(base_draw.x, ( base_draw.y + dilat - 1 ) , dilat - 1, (size_y * dilat) + 1);
fill_rect(base_draw.x +((size_x + 1) * dilat), base_draw.y + dilat - 1, dilat - 1 , (size_y* dilat ) + 1 );
for i = 0 to size_x 
do
  for j = 0 to size_y 
  do
    draw_absolute_pt  ( { x = i ; y = j } , base_draw , dilat , black )
  done;
done
;;


(* ***************** Xavier *********************** *)
(* Choisie al�atoirement une couleur *)
let color_choice(t : t_color t_array) : t_color =
  t.value.(rand_int(0,6)) 
;;

let cur_shape_choice(shapes, mat_szx, mat_szy, color_arr : t_shape t_array * int * int * t_color t_array) : t_cur_shape =
  let rand_shape : int = rand_int(0,17)
  and rand_color : t_color = color_choice(color_arr)
  and point : t_point = {x = mat_szx/2; y = mat_szy} in
  ({base = ref point; shape = ref (rand_shape); color = ref rand_color})
;;


let point(cur, p : t_cur_shape * t_point) : t_point =
  let base : t_point ref  = cur.base in  
  let base_x : int = !base.x
  and base_y : int = !base.y in
  {x = base_x + p.x; y = base_y + p.y}
;;

let rec insert_aux(cur, shape, my_mat : t_cur_shape * t_point list * t_color matrix) : bool =
  if(shape = [])
  then true
  else
    let p : t_point = point(cur, fst(shape)) in
    if(my_mat.(p.x-1).(p.y-1) <> 16777215)
    then false
    else insert_aux(cur, rem_fst(shape), my_mat)
;;
 
let insert(cur, shape, param, my_mat : t_cur_shape * t_point list * t_param * t_color matrix) : bool =
    insert_aux(cur, shape, my_mat);
;;


let init_play_aux (play : t_play ) : unit =
if ( insert( cur_shape_choice(init_shapes(), param_mat_szx(init_param())-1 ,
                                param_mat_szy(init_param())-1 , init_color()) ,
               shape_shape ( (t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(play)))) ) ,
               init_param(), play.mat) )
   
  then (
        draw_frame(play.par.graphics.base, play.par.mat_szx, play.par.mat_szy, play.par.graphics.dilat);
        drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(play))))) ,
                           {x = (param_mat_szx(init_param())/2)+1 ; y = param_mat_szy(init_param())} ,
                           { x = 50 ; y = 50 }, 20, color_choice (init_color())) )
  else failwith("error")
;;

let init_play () : t_play =
  let play : t_play = {
      par = init_param() ;
    cur_shape = (cur_shape_choice( init_shapes() , param_mat_szx(init_param()) ,
                                   param_mat_szy(init_param()) , init_color()) );
    mat = mat_make (param_mat_szx(init_param()), param_mat_szy(init_param()), white)
    } 
  in
  init_play_aux (play);
  play
;;

(* ****************** theo *************************** *)

let valid_matrix_point ( p , param : t_point * t_param ) : bool =
  if ( ( p.x < param_mat_szx(param) && p.x >= 0 )
       && ( p.y < param_mat_szy(param) && p.y >= 0 ) )
  then true
  else false
;;

let rec is_free_move_aux ( p , shape , my_mat , param : t_point * t_point list * t_color matrix * t_param ) : bool =
  if shape = []
  then true
  else
    if valid_matrix_point ( fst(shape) , param ) = false
    then false
    else is_free_move_aux ( p , rem_fst(shape) , my_mat , param );
;;

let is_free_move ( p , shape , my_mat , param : t_point * t_point list * t_color matrix * t_param ) : bool =
  is_free_move_aux ( p , shape , my_mat , param )
;;

let move_left ( pl : t_play ) : unit =
  let a : t_point ref =  cur_shape_base(play_cur_shape(pl))
  and b : t_shape array = t_array_value (init_shapes())
  and c : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  if is_free_move ( {x= -1; y= 0} , shape_shape(b.(!c)) ,
                   play_mat(pl) , pl.par)
  then
    (
      drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(pl))))) ,
                         {x = (!a.x+1) ; y = !a.y+1} , { x = 50 ; y = 50 }, 20 , white );
      drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(pl))))) ,
                         {x = !a.x ; y = !a.y} , { x = 50 ; y = 50 }, 20 , !(pl.cur_shape.color) );
      a:= { x = !a.x-1 ; y = !a.y };
   
    )
    else failwith "cc"
;;


let move_right ( pl : t_play ) : unit =
   let a : t_point ref =  cur_shape_base(play_cur_shape(pl))
  and b : t_shape array = t_array_value (init_shapes())
  and c : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
   if is_free_move ( {x= -1; y= 0} , shape_shape(b.(!c)) ,
                     play_mat(pl) , pl.par)
   then(
      drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(pl))))) ,
                           {x = (!a.x-1) ; y = !a.y+1} , { x = 50 ; y = 50 }, 20 , white );
   drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(pl))))) ,
                      {x = !a.x+1 ; y = !a.y} , { x = 50 ; y = 50 }, 20 , !(pl.cur_shape.color) );
   a:= { x = !a.x+1 ; y = !a.y };
    )
   else ()
;;

let move_down ( pl : t_play ) : bool =
  let a : t_point ref =  cur_shape_base(play_cur_shape(pl))
  and b : t_shape array = t_array_value (init_shapes())
  and c : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  if is_free_move ( !a , shape_shape(b.(!c)) ,
                    play_mat(pl) , pl.par)
  then (
    drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(pl))))) ,
                           {x = (!a.x) ; y = !a.y+1} , { x = 50 ; y = 50 }, 20 , white );
    drawfill_pt_list ( shape_shape((t_array_value (init_shapes())).(!(cur_shape_shape(play_cur_shape(pl))))) ,
                           {x = !a.x ; y = !a.y} , { x = 50 ; y = 50 }, 20 , !(pl.cur_shape.color) );
    a:= {x= !a.x ; y= !a.y-1 };
    true
  )
  else false
;;

let rotate_left ( pl : t_play ) : unit =
  let a : t_shape array = t_array_value (init_shapes())
  and b : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  drawfill_pt_list (shape_shape(a.(shape_lft_shape_rot (a.(!b)))) ,
                    shape_lft_base_rot(a.(!b)) ,{x = 50 ; y = 50}  , 20 , red)
;;

let rotate_right ( pl : t_play ) : unit =
   let a : t_shape array = t_array_value (init_shapes())
  and b : int ref =( cur_shape_shape( play_cur_shape(pl) ) ) in
  drawfill_pt_list (shape_shape(a.(shape_rgt_shape_rot (a.(!b)))) ,
                    shape_rgt_base_rot(a.(!b)) ,{x = 50 ; y = 50}  , 20 , color_choice(init_color()))
;;

let move_at_bottom ( pl : t_play ) : unit =
let a : bool ref = ref (move_down(pl)) in
  while !a = true
  do
    a:= move_down (pl)
  done
;;

(* ----------------------------------------------- *)
(* ----------------------------------------------- *)
(*    Deplacements et controle des deplacements    *)
(* ----------------------------------------------- *)
(* ----------------------------------------------- *)

(* choix des deplacements suivant le caractere saisi *)

let move(pl, dir : t_play * char) : bool = 
  (
  if dir = 'z'
    then rotate_right(pl)
    else
      if dir = 's'
      then rotate_left(pl)
      else
        if dir = 'q'
        then move_left(pl)
        else
          if dir = 'd'
          then move_right(pl)
          else () ;  
  (dir = 'v')
  )
;;
 
(* ----------------------------------- *)
(* ----------------------------------- *)
(*    Suppression des lignes pleines   *)
(* ----------------------------------- *)
(* ----------------------------------- *)

let is_column_full ( my_mat , y , size_x : t_color matrix * int * int ) : bool =
  let is_full : bool ref = ref false
  and szx : int = size_x - 1
  and line_full : int ref = ref 0 in
  for i = 0 to szx
  do
    if my_mat.(y).(i) <> white
    then(
      line_full := !line_full + 1;
      if !line_full = 15
      then
        is_full := true
      else
        is_full := !is_full
    )
  done ;
  !is_full
;;

let decal ( my_mat , y , size_x , size_y , param : t_color matrix * int * int * int * t_param ) : unit =
  let a : int = size_x - 1
  and b :int = size_y - 2 in
    for i = y to b
    do
      for j = 0 to a
      do
      my_mat.(j).(i) <- my_mat.(j).(i+1)
      done;
    done
;;

let clear_play ( pl : t_play ) : unit =
  let a : bool ref = ref false in
  for i = 0 to ( param_mat_szy(play_par(pl)) - 1 )
  do
    a:= is_column_full ( play_mat(pl) , i , param_mat_szx(play_par(pl)) );
    while !a = true
    do
      for j = 0 to ( param_mat_szx(play_par(pl)) - 1 )
      do
        (play_mat(pl)).(i).(j) <- white
      done;
      a:= false
    done;
  done
;;

let final_insert ( cur , shape , my_mat : t_cur_shape * t_point list * t_color matrix ) : unit =
  let a : t_point ref =  cur_shape_base(cur) in
    my_mat.( !a.x ).( !a.y ) <- !(cur.color)
;;
                                             
let final_newstep(pl : t_play) : bool =
  let shape : t_point list = shape_shape( (t_array_value(init_shapes())).(!(cur_shape_shape(play_cur_shape(pl)))) )
  and p : t_point ref = cur_shape_base(play_cur_shape(pl)) in
    if is_free_move(!p, shape, play_mat(pl), init_param())
    then ()
    else final_insert(play_cur_shape(pl), shape, play_mat(pl));    
    if is_column_full( play_mat(pl), !p.y , param_mat_szx(play_par(pl))-1 )
    then(
      clear_play(pl);
      decal( play_mat(pl) , !p.y , param_mat_szx(play_par(pl))-1 , param_mat_szy(play_par(pl))-1 , play_par(pl));
      true )
    else false
;;


(*
cur_shape_choice( param_shapes(play_par(pl)) , param_mat_szx(play_par(pl))-1 , param_mat_szy(play_par(pl))-1 , init_color() )
 *)

(* --------------------- *)
(* --------------------- *)
(*   Une etape de jeu    *)
(* --------------------- *)
(* --------------------- *)

let newstep(pl, new_t, t, dt : t_play * float ref * float * float) : bool = 
  let the_end : bool ref = ref (!new_t -. t > dt) and dec : bool ref = ref false in
  let dir : char ref = ref 'x' and notmove : bool ref = ref false in
    (
    while not(!the_end)
    do 
      if key_pressed()
      then dir := read_key()
      else () ;
      dec := move(pl, !dir) ;
      dir := 'x' ; 
      new_t := Sys.time() ;
      the_end := !dec || (!new_t -. t > dt) ;
    done ; 
    if !dec 
    then (move_at_bottom(pl) ; notmove := true)
    else notmove := not(move_down(pl)) ;
    if !notmove
    then the_end := final_newstep(pl)
    else the_end := false;
    !the_end ;
    )
;;

(* ------------------------ *)
(* ------------------------ *)
(*    Fonction principale   *)
(* ------------------------ *)
(* ------------------------ *)


let jeuCP2() : unit =
  let pl : t_play = init_play() in
  let t : float ref = ref (Sys.time()) and new_t : float ref = ref (Sys.time()) in
  let dt : float ref = ref (time_init(pl.par)) and t_acc : float ref = ref (Sys.time()) in
  let the_end : bool ref = ref false in
    while not(!the_end)
    do
      the_end := newstep(pl, new_t, !t, !dt) ; 
      if ((!new_t -. !t_acc) > time_extent(pl.par))
      then 
        (
        dt := !dt *. time_ratio(pl.par) ; 
        t_acc := !new_t
        ) 
      else () ;
      t := !new_t
    done
;;


clear_graph();;
