clear_graph();;

(*********************** Theo *******************************)
(* La barre *)

let part_barre_h : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=3 ; y=0 }];;
let part_barre_v : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=0 ; y=3 }];;

drawfill_pt_list ( part_barre_h , {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
drawfill_pt_list ( part_barre_v , {x=0 ; y=0} , {x=50 ; y=50} , 20 , green );;

(* Le cube *)

let part_cube : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 }];;

drawfill_pt_list ( part_cube , {x=0 ; y=0} , {x=50 ; y=50} , 20 , cyan );;

(*********************** Xavier *******************************)

(* Le 'L' *)

let part_L_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 }];;
let part_L_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=2 ; y=1 }];;
let part_L_3 : t_point list = [{x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 } ; {x=0 ; y=2 }];;
let part_L_4 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=2 ; y=1 }];;

drawfill_pt_list ( part_L_1 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_L_2 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_L_3 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_L_4 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;

(* Le 'J' *)

let part_J_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;
let part_J_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=0 }];;
let part_J_3 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=2 }];;
let part_J_4 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=2 ; y=1 } ; {x=2 ; y=0 }];;

drawfill_pt_list ( part_J_1 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_J_2 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_J_3 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_J_4 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;

(* Le S-Tetrimino *)

let part_S_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=2 ; y=1 }];;
let part_S_2 : t_point list = [{x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=0 } ; {x=1 ; y=1 }];;

clear_graph();;
drawfill_pt_list ( part_S_1 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_S_2 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;

(* Le Z-Tetrimino *) 

let part_Z_1 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=0 }];;
let part_Z_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;

clear_graph();;
drawfill_pt_list ( part_Z_1 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_Z_2 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;

(* le T-Tetrimino *) 

let part_T_1 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=1 }];;
let part_T_2 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;
let part_T_3 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=2 ; y=0 }];;
let part_T_4 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=1 }];;

drawfill_pt_list ( part_T_1 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_T_2 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_T_3 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
drawfill_pt_list ( part_T_4 , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;
clear_graph();;
