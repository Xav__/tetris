
open_graph(500,500);;
draw_rect(50,50,50,50);;

fill_rect(50,50,50,50);;
clear_graph();;



(* Question 1 *)
(* Trace le rectangle d'un carr� *)
let draw_absolute_pt(p, base_draw, dilat, col : t_point* t_point * int * t_color) : unit =
  set_color(col);
  let d : int = dilat - 1 in
  draw_rect(base_draw.x + (p.x * dilat), base_draw.y + (p.y * dilat), d, d)
;;

clear_graph();;
draw_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, black);;
draw_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, black);;
draw_absolute_pt(({x = 0 ; y = 1}), ({x = 50 ; y = 50}), 20, black);;

(* Remplir un rectangle *)
let fill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  set_color(col);
  let d : int = dilat - 3 in
  fill_rect(base_draw.x + (p.x * dilat)+1, base_draw.y + (p.y * dilat) +1, d, d)
;;
clear_graph();;
fill_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
fill_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;

(* tracer un rectangle et le remplir *)
let drawfill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  draw_absolute_pt(p, base_draw, dilat, black);
  fill_absolute_pt(p, base_draw, dilat, red)
;;

drawfill_absolute_pt(({x = 0 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
drawfill_absolute_pt(({x = 1 ; y = 0}), ({x = 50 ; y = 50}), 20, red);;
drawfill_absolute_pt(({x = 0 ; y = 1}), ({x = 50 ; y = 50}), 20, red);;


(* Question 7 *)
(* Choisie al�atoirement une couleur *)
let color_choice(t : t_color t_array) : t_color =
  t.value.(rand_int(0,6)) 
;;

color_choice(init_color());;

(* Cr�er un type t_cur_shape *)
let cur_shape_choice(shapes, mat_szx, mat_szy, color_arr : t_shape t_array * int * int * t_color t_array) : t_cur_shape =
  let rand_shape : int = rand_int(0,6)
  and rand_color : t_color = color_choice(color_arr)
  and point : t_point = {x = mat_szx/2; y = mat_szy-1} in
  ({base = ref point; shape = ref (rand_shape); color = ref rand_color})
;;

cur_shape_choice(init_shapes(), 15, 28, init_color());;

(* renvoie les points (x, y) d'une pi�ce par le biais de son base point *)
let point(cur, p : t_cur_shape * t_point) : t_point =
  let base : t_point ref  = cur.base in  
  let base_x : int = !base.x
  and base_y : int = !base.y in
  {x = base_x + p.x; y = base_y + p.y}
;;

#trace point;;
point(cur_shape_choice(init_shapes(), 15, 28, init_color()), {x =0 ; y = -1});;
point(cur_shape_choice(init_shapes(), 15, 28, init_color()), {x =0 ; y = 1});;
              
(* Test si la pi�ce peut �tre ins�rer *)  
let rec insert_aux(cur, shape, my_mat : t_cur_shape * t_point list * t_color matrix) : bool =
  if(shape = [])
  then true
  else
    let p : t_point = point(cur, fst(shape)) in
    if(my_mat.(p.y).(p.x) <> white)
    then false
    else insert_aux(cur, rem_fst(shape), my_mat)
;;

#trace insert_aux;;
insert_aux(cur_shape_choice(init_shapes(), 15, 28, init_color()), part_cube, create_matrix(init_param()));;
insert_aux(cur_shape_choice(init_shapes(), 15, 28, init_color()), part_cube, a);;

(* cr�er une matrice avec des 0 *)
let create_matrix(param: t_param): t_color matrix =
  mat_make(param_mat_szy(param), param_mat_szx(param), white)
;;

let a : t_color matrix = create_matrix(init_param());;
a.(27).(7);;
a.(27).(7) <- 1;;
a.(27).(7) <- 16777215;;
a.(27).(8);;
a.(27).(8) <- 1;;

(* Test si la pi�ce peut �tre ins�rer *)  
let insert(cur, shape, param, my_mat : t_cur_shape * t_point list * t_param * t_color matrix) : bool =
  insert_aux(cur, shape, my_mat)
;;

insert(cur_shape_choice(init_shapes(), 14, 27, init_color()), part_cube, init_param(), create_matrix(init_param()));;
insert(cur_shape_choice(init_shapes(), 14, 27, init_color()), part_cube, init_param(),  a);;
