
(* ****************** Xavier ********************* 
(** Fonction test insert
     @author Xavier
*)
let test_insert(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"insert") in
  let test_result : bool t_test_result = test_exec(test_step, insert, (cur_shape_choice(init_shapes(), 15, 27, init_color()), part_cube, init_param(), create_matrix(init_param()))) in
                                                   
    (
    assert_true(test_step, "succes", test_is_success(test_result));
    test_end(test_step)
    )
;;*)

(** Fonction test insert
     @author Xavier
     @param test matrice vide (que du blanc)
*)
(* test valeur incorrect *)
let test_insert_a(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"insert_a")
  and param : t_param = init_param() in
  let arr_param_shape : t_shape t_array = param.shapes in
  let cur : t_cur_shape = cur_shape_choice(arr_param_shape, param.mat_szx-1, param.mat_szy-1, init_color()) in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x) in
  let pt_l : t_point list = shape.shape 
  and my_mat : t_color matrix= mat_make(param.mat_szx, param.mat_szy, white) in
  let test_result : bool t_test_result = test_exec(test_step, insert, (cur, pt_l, param, my_mat)) in
  (
    assert_true(test_step, "La pi�ce peut �tre ins�r�e", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test insert
     @author Xavier
     @param test matrice pleine (que du jaune)
*)
(* test valeur incorrect *)
let test_insert_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"insert_b")
  and param : t_param = init_param() in
  let arr_param_shape : t_shape t_array = param.shapes in
  let cur : t_cur_shape = cur_shape_choice(arr_param_shape, param.mat_szx-1, param.mat_szy-1, init_color()) in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x) in
  let pt_l : t_point list = shape.shape 
  and my_mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, yellow) in
  let test_result : bool t_test_result = test_fail_exec(test_step, insert, (cur, pt_l, param, my_mat)) in
  (
    assert_false(test_step, "La pi�ce ne doit pas �tre ins�r�e", test_is_success(test_result));
    test_end(test_step)
  )
;;

let param : t_param = init_param();;
insert(cur_shape_choice(param.shapes, param.mat_szx-1, param.mat_szy-1, init_color()), part_cube,
       param, mat_make(param.mat_szx, param.mat_szy, yellow));;
      

(** Fonction test valid_matrix_point
     @author Xavier
 *)
let test_valid_matrix_point(status : t_test_status) : unit =
  let test_step = test_start(status, "valid_matrix_point")
  and point : t_point = {x = 0 ; y = 0}
  and param : t_param = init_param() in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "Point [0;0] appartenant bien � la matrice", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step)  
  )
;;

(** Fonction test valid_matrix_point
     @author Xavier
*)
(* test valeur correct *)
let test_valid_matrix_point_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"valid_matrix_point_a")
  and point : t_point = {x = 14 ; y = 27}
  and param : t_param = init_param() in
  let test_result : bool t_test_result = test_exec(test_step, valid_matrix_point, (point, param)) in
    (
    if test_is_success(test_result)
    then assert_equals(test_step, "Point [15;24] appartenant bien � la matrice", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step)
    )
;;

(** Fonction test valid_matrix_point
     @author Xavier
*)
(* test valeur incorrect *)
let test_valid_matrix_point_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"valid_matrix_point_b")
  and point : t_point = {x = 14 ; y = 30}
  and param : t_param = init_param() in
  let test_result : bool t_test_result = test_fail_exec(test_step, valid_matrix_point, (point, param)) in
  (
    assert_false(test_step, "{14;30} n'appartient pas � la matrice", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test is_free_move
     @author Xavier
     @param test une pi�ce au hasard dans une matrice vide (qui contient que du blanc)
*)
(* test correct *)
let test_is_free_move_a(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_free_move_a")
  and param : t_param = init_param() in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x)
  and mat: t_color matrix = mat_make(param.mat_szx, param.mat_szy, white) in
  let test_result : bool t_test_result = test_exec(test_step, is_free_move, (fst(shape.shape), shape.shape,
                                           mat, param)) in
  (
   assert_true(test_step, "La pi�ce peut bien bouger", test_is_success(test_result));
    test_end(test_step)
  )
;;
let param : t_param = init_param();;
is_free_move(fst(part_cube), part_cube, mat_make(param.mat_szx, param.mat_szy, white), param);;

(** Fonction test is_free_move
     @author Xavier
     @param test une pi�ce au hasard dans une matrice qui remplie (contient que du jaune)
*)
(* test correct *)
let test_is_free_move_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_free_move_b")
  and param : t_param = init_param() in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, yellow)in
  let test_result : bool t_test_result = test_fail_exec(test_step, is_free_move, (fst(shape.shape), shape.shape,
                                           mat, param)) in
  (
    assert_false(test_step, "La pi�ce ne peut pas �tre ins�r�e", test_is_success(test_result));
    test_end(test_step)
  )
;;

(** Fonction test valid_cur_shape_choice
     @author Xavier
*)
(* test valeur correct 
let test_test_cur_shape_choice_a(status : t_test_status) : unit = 
  let test_step : t_test_step = test_start(status,"cur_shape_choice_a")
  and param : t_param = init_param()
  and shapes : t_shape t_array = init_shapes() in
  let mat_szx : int = param_mat_szx(param)
  and mat_szy : int = param_mat_szy(param) in
  let arr_shape : t_shape t_array = param.shapes in
  let x : int = rand_int(0, arr_shape.len -1) in
  let shape : t_shape = arr_shape.value.(x)
  let mat : t_color matrix = mat_make(mat_szx, mat_szy, white) in
      let test_result : bool t_test_result = test_exec(test_step, cur_shape_choice, (shapes,
                                   mat_szx, mat_szy, init_color(), param, mat)) in
    (
    if test_is_success(test_result)
    then assert_equals(test_step, "t_cur_shape", test_get(test_result), )
    else test_error(test_step) ;
    test_end(test_step)
    )
;;
let shapes : t_shape t_array = init_shapes();;*)

(** Fonction test is_column_full
     @author Xavier
     @param test si une ligne al�toire est pleine dans une matrice 'vide' (qui contient que du blanc)
*)
let test_is_column_full_a(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_column_full_a")
  and param : t_param = init_param() in
  let size_x : int = param.mat_szx
  and y : int = rand_int(0, param.mat_szx -1)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, white)in
  let test_result : bool t_test_result = test_exec(test_step, is_column_full, (mat, y, size_x)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "La colonne est vide -> renvoie false", test_get(test_result), false)
    else test_error(test_step) ;
    test_end(test_step)
  )
;;

is_column_full(mat_make(15,28, white), 5, 15);;

(** Fonction test is_column_full
     @author Xavier
     @param test si une ligne al�toire est pleine dans une matrice 'pleine' (qui contient que du jaune)
*)
let test_is_column_full_b(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_column_full_b")
  and param : t_param = init_param() in
  let size_x : int = param.mat_szx
  and y : int = rand_int(0, param.mat_szx -1)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, yellow)in
  let test_result : bool t_test_result = test_exec(test_step, is_column_full, (mat, y, size_x)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "La colonne doit �tre pleine -> renvoie true", test_get(test_result), true)
    else test_error(test_step) ;
    test_end(test_step) 
  )
;;

(** Fonction test is_column_full
     @author Xavier
     @param test si une ligne al�toire est pleine dans une matrice qui � la m�me ligne test�e est partiellement remplie (derni�re case de la ligne vide)
*)
(* test correct *)
let test_is_column_full_c(status : t_test_status) : unit =
  let test_step : t_test_step = test_start(status,"is_column_full_c")
  and param : t_param = init_param() in
  let size_x : int = param.mat_szx
  and y : int = rand_int(0, param.mat_szx -1)
  and mat : t_color matrix = mat_make(param.mat_szx, param.mat_szy, white)in
  for i=0 to size_x-2
  do
    mat.(y).(i) <- yellow
  done;
  let test_result : bool t_test_result = test_exec(test_step, is_column_full, (mat, y, size_x)) in
  (
    if test_is_success(test_result)
    then assert_equals(test_step, "La colonne n'est que partiellement 'pleine' -> renvoie false", test_get(test_result), false)
    else test_error(test_step) ;
    test_end(test_step) 
  )
;;


let test_run() : unit =
  let alltests : t_test_status = create_test_status() in
  (

    (* test de insert *)
    test_insert_a(alltests);
    test_insert_b(alltests);

    (* test de cur_shape_choice 
    test_cur_shape_choice_a(alltests);*)

    (* test de valid_matrix_point *)
    test_valid_matrix_point(alltests);
    test_valid_matrix_point_a(alltests);
    test_valid_matrix_point_b(alltests);

    (* test de is_free_move*)
    test_is_free_move_a(alltests);
    test_is_free_move_b(alltests);

    (* test is_column_full *)
    test_is_column_full_a(alltests);
    test_is_column_full_b(alltests);
    test_is_column_full_c(alltests);
    
    (* Print test status at the end *)
    print_test_report(alltests)
  )
;;

test_run();;

