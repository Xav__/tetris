

(*********************** Theo *******************************)

(* La barre *)

let part_barre_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=3 ; y=0 }];;
let part_barre_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=0 ; y=3 }];;

(* Le cube *)

let part_cube : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 }];;

(*********************** Xavier *******************************)

(* Le 'L' *)

let part_L_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 }];;
let part_L_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=2 ; y=1 }];;
let part_L_3 : t_point list = [{x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 } ; {x=0 ; y=2 }];;
let part_L_4 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=2 ; y=1 }];;

(* Le 'J' *)

let part_J_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;
let part_J_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=0 }];;
let part_J_3 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=2 }];;
let part_J_4 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=2 ; y=1 } ; {x=2 ; y=0 }];;

(* Le S-Tetrimino *)

let part_S_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=2 ; y=1 }];;
let part_S_2 : t_point list = [{x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=0 } ; {x=1 ; y=1 }];;

(* Le Z-Tetrimino *) 

let part_Z_1 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=0 }];;
let part_Z_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;

(* le T-Tetrimino *) 

let part_T_1 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=1 }];;
let part_T_2 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;
let part_T_3 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=2 ; y=0 }];;
let part_T_4 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=1 }];;

