(* open CPutil;; *)

(* -------------------------- *)
(* -------------------------- *)
(*    fonctions utilitaires   *)
(* -------------------------- *)
(* -------------------------- *)

(** {3 Fonctions utilitaires} *)

let mywait(x : float) : unit =
  let y : float ref = ref (Sys.time()) in
  while (Sys.time() -. !y) < x
  do ()
  done
;;


(* ------------------------------------------------- *)
(* ------------------------------------------------- *)
(*    Types, formes, parametrage et initialisation   *)
(* ------------------------------------------------- *)
(* ------------------------------------------------- *)

(** {3 Types, formes, parametrage et initialisation} *)

(* Types *)
(** {4 Types} *)

type t_point = {x : int; y : int} ;;

type 'a t_array = {len : int ; value : 'a array} ;;

type t_shape = {shape : t_point list ; x_len : int ; y_len : int ; 
                rot_rgt_base : t_point ; rot_rgt_shape : int ; 
                rot_lft_base : t_point ; rot_lft_shape : int} ;; 

type t_cur_shape = {base : t_point ref ; shape : int ref ; color : t_color ref} ;;


type t_param_time = {init : float ; extent : float ; ratio : float} ;;

type t_param_graphics = 
    {base : t_point ; dilat : int ; color_arr : t_color t_array} ;;

type t_param = 
  {time : t_param_time ; 
   mat_szx : int ; mat_szy : int ;
   graphics : t_param_graphics ; 
   shapes : t_shape t_array
} ;;

type t_play = {par : t_param ; cur_shape : t_cur_shape ; mat : t_color matrix} ;;

(* Formes - t_point list et init t_shape *)

(* La barre *)

let part_barre_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=3 ; y=0 }];;
let part_barre_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=0 ; y=3 }];;

(* Le cube *)

let part_cube : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 }];;

(* Le 'L' *)

let part_L_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 }];;
let part_L_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=2 ; y=1 }];;
let part_L_3 : t_point list = [{x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 } ; {x=0 ; y=2 }];;
let part_L_4 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=2 ; y=0 } ; {x=2 ; y=1 }];;

(* Le 'J' *)

let part_J_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;
let part_J_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=0 }];;
let part_J_3 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=2 }];;
let part_J_4 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=2 ; y=1 } ; {x=2 ; y=0 }];;

(* Le S-Tetrimino *)

let part_S_1 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=2 ; y=1 }];;
let part_S_2 : t_point list = [{x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=0 } ; {x=1 ; y=1 }];;

(* Le Z-Tetrimino *) 

let part_Z_1 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=0 }];;
let part_Z_2 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;

(* le T-Tetrimino *) 

let part_T_1 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=1 } ; {x=1 ; y=0 } ; {x=2 ; y=1 }];;
let part_T_2 : t_point list = [{x=0 ; y=1 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=1 ; y=2 }];;
let part_T_3 : t_point list = [{x=0 ; y=0 } ; {x=1 ; y=0 } ; {x=1 ; y=1 } ; {x=2 ; y=0 }];;
let part_T_4 : t_point list = [{x=0 ; y=0 } ; {x=0 ; y=1 } ; {x=0 ; y=2 } ; {x=1 ; y=1 }];;


let init_barre_1() : t_shape = 
  {shape = part_barre_1 ; 
  x_len = 4 ; y_len = 1 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 1 ; 
  rot_lft_base = {x = 0 ; y = 0} ; rot_lft_shape = 1} 
;;

let init_barre_2() : t_shape = 
  {shape = part_barre_2 ; 
  x_len = 1 ; y_len = 4 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 0 ; 
  rot_lft_base = {x = 0 ; y = 0} ; rot_lft_shape = 0} 
;;

let init_cube() : t_shape = 
  {shape = part_cube ; 
  x_len = 2 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 2 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 2} 
;;

let init_L_1() : t_shape = 
  {shape = part_L_1 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 4 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 6} 
;;

let init_L_2() : t_shape = 
  {shape = part_L_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 5 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 3} 
;;
let init_L_3() : t_shape = 
  {shape = part_L_3 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 6 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 4} 
;;

let init_L_4() : t_shape = 
  {shape = part_L_4 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 3 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 5} 
;;

let init_J_1() : t_shape = 
  {shape = part_J_1 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 8 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 10} 
;;

let init_J_2() : t_shape = 
  {shape = part_J_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 9 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 7} 
;;

let init_J_3() : t_shape = 
  {shape = part_J_3 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 10 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 8} 
;;

let init_J_4() : t_shape = 
  {shape = part_J_4 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 7 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 9} 
;;

let init_S_1() : t_shape = 
  {shape = part_S_1 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 12 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 12} 
;;

let init_S_2() : t_shape = 
  {shape = part_S_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 11 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 11} 
;;

let init_Z_1() : t_shape = 
  {shape = part_Z_1 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 14 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 14} 
;;

let init_Z_2() : t_shape = 
  {shape = part_Z_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 13 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 13} 
;;

let init_T_1() : t_shape = 
  {shape = part_T_1 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 16 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 18} 
;;

let init_T_2() : t_shape = 
  {shape = part_T_2 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 17 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 15} 
;;

let init_T_3() : t_shape = 
  {shape = part_T_3 ; 
  x_len = 3 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 18 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 16} 
;;

let init_T_4() : t_shape = 
  {shape = part_T_4 ; 
  x_len = 2 ; y_len = 3 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 15 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 17} 
;;


(* ****************** theo *************************** *)
(* ************ extraction de parametres ************* *)
let shape_shape ( a : t_shape ) : t_point list =
  a.shape
;;

let shape_xlen ( a : t_shape ) : int =
  a.x_len
;;

let shape_ylen ( a : t_shape ) : int =
  a.y_len
;;

let shape_rgt_base_rot ( a : t_shape ) : t_point =
  a.rot_rgt_base
;;

let shape_lft_base_rot ( a : t_shape ) : t_point =
  a.rot_lft_base
;;

let shape_rgt_shape_rot ( a : t_shape ) : int =
  a.rot_rgt_shape
;;

let shape_lft_shape_rot( a : t_shape ) : int =
  a.rot_rgt_shape
;;

let cur_shape_base( b : t_cur_shape ) : t_point ref  =
  b.base
;;

let cur_shape_shape( b : t_cur_shape ) : int ref =
  b.shape
;;

let cur_shape_color( b : t_cur_shape ) : t_color ref =
  b.color
;;

(*
let param_time_init ( c : t_param_time ) : float =

;;
 *)

let param_time_init ( c : t_param_time ) : float =
  c.init
;;

let param_time_extent ( c : t_param_time ) : float =
  c.extent
;;

let param_time_ratio ( c : t_param_time ) : float =
  c.ratio
;;

let param_graphics_base ( d : t_param_graphics ) : t_point =
  d.base
;;

let param_graphics_dilat ( d : t_param_graphics ) : int =
  d.dilat
;;

let param_time ( e : t_param ) : t_param_time =
  e.time
;;

let param_mat_szx ( e : t_param ) : int =
  e.mat_szx
;;
let param_mat_szy ( e : t_param ) : int =
  e.mat_szy
;;

let param_graphics_color_len( e : t_param ) : int =
  e.graphics.color_arr.len
;;

let param_shapes ( e : t_param ) : t_shape t_array  =
  e.shapes
;;

let play_par ( f : t_play ) : t_param =
  f.par
;;

let play_cur_shape ( f : t_play ) : t_cur_shape =
  f.cur_shape
;;

let play_mat ( f : t_play ) : t_color matrix =
  f.mat
;;

(* crer une matrice avec des 0 *)
let create_matrix(param: t_param): t_color matrix =
  mat_make(param_mat_szy(param), param_mat_szx(param), black)
;;


(* Initialisation de quelques formes et des parametres *)
(** {4 Initialisation de quelques formes et des parametres} *)

let init_sh011() : t_shape = 
  {shape = [{x = 0 ; y = 0} ; {x = 1 ; y = 0} ; {x = 2 ; y = 0} ; {x = 3 ; y = 0}] ; 
  x_len = 4 ; y_len = 1 ; 
  rot_rgt_base = {x = 1 ;  y = 1} ; rot_rgt_shape = 1 ; 
  rot_lft_base = {x = 2 ; y = 1} ; rot_lft_shape = 1} 
;;
let init_sh112() : t_shape = 
  {shape = [{x = 0 ; y = 0} ; {x = 0 ; y = -1} ; {x = 0 ; y = -2} ; {x = 0 ; y = -3}] ; 
  x_len = 1 ; y_len = 4 ; 
  rot_rgt_base = {x = -2 ;  y = -1} ; rot_rgt_shape = 0 ; 
  rot_lft_base = {x = -1 ; y = -1} ; rot_lft_shape = 0} 
;;
let init_sh211() : t_shape = 
  {shape = [{x = 0 ; y = 0} ; {x = 0 ; y = -1} ; {x = 1 ; y = 0} ; {x = 1 ; y = -1}] ; 
  x_len = 2 ; y_len = 2 ; 
  rot_rgt_base = {x = 0 ;  y = 0} ; rot_rgt_shape = 2 ; 
  rot_lft_base = {x = 0 ;  y = 0} ; rot_lft_shape = 2} 
;;

let init_shapes() : t_shape t_array = 
  {len = 3 ; value = [| init_sh011() ; init_sh112() ; init_sh211() |]} 
;;
let init_color() : t_color t_array = 
  {len = 7 ; value = [|blue ; red ; green ; yellow ; cyan ; magenta ; grey|]} ;;

let init_param() : t_param = 
    {
    time = {init = 1.0 ; extent = 10.0 ; ratio = 0.8} ; 
    mat_szx = 15 ; mat_szy = 28 ;
    graphics = {base = {x = 50 ; y = 50} ; dilat = 20 ; color_arr = init_color()} ; 
    shapes = init_shapes()
    }
;;

let init_play () : t_play =
  {
    par= init_param() ;
    cur_shape = {base= ref (fst(shape_shape (init_barre_1()))) ; shape= ref 1 ; color=ref black };
    mat = mat_make ( param_mat_szx(init_param()) , param_mat_szy(init_param()) , 16777215 )
  }
;;



(****** Opengraph *******)

open_graph ( 800 , 800 ) ;;

(** {4 Fonctions} *)

(* ******************* Theo ********************** *)
(** Fonction ...........
     @author Theo
*)

let rec workspace ( a, b, l : int * int * t_point list ) : t_point list =
  if b < 24
  then
    (
      if a <= 14
      then
        workspace ( a+1, b, add_lst ( l , { x= a ; y= b } ) )
      else
        workspace ( 0, b+1, l )
    )
  else
    l
;;

workspace (0 , 0 , []);;
(*
draw_pt_list ( workspace (0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );;

clear_graph();;
drawfill_pt_list ( workspace(0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
 *)

(* --------------------------------- *)
(* --------------------------------- *)
(*   Types et fonctions graphique    *)
(* --------------------------------- *)
(* --------------------------------- *)

(** {3 Types et Fonctions graphique} *)

type t_cur_shape = {base : t_point ref ;
shape : int ref ; color : t_color ref} ;;


(* ***************** Xavier *********************** *)
(** Fonction : Trace le rectangle d'un carr
    @author Xavier
 *)

let draw_absolute_pt(p, base_draw, dilat, col : t_point* t_point * int * t_color) : unit =
  let d : int = dilat - 1 in
  (
  set_color(col);
  draw_rect(base_draw.x + (p.x * dilat), base_draw.y + (p.y * dilat), d, d)
  )
  ;;

(* ***************** Xavier *********************** *)
(** Fonction : Remplir un rectangle
    @author Xavier
 *)
  
let fill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  let d : int = dilat - 3 in
  (
  set_color(col);
  fill_rect(base_draw.x + (p.x * dilat)+1, base_draw.y + (p.y * dilat) +1, d, d)
  )
  ;;

(* ***************** Xavier *********************** *)
(** Fonction : tracer un rectangle et le remplir 
    @author Xavier
 *)
  
let drawfill_absolute_pt(p, base_draw, dilat, col : t_point * t_point * int * t_color) : unit =
  draw_absolute_pt(p, base_draw, dilat, black);
  fill_absolute_pt(p, base_draw, dilat, red)
;;

(* ***************** Xavier & Lucie *********************** *)
(** Fonction
     @author Xavier & Lucie
*)

let sum_point(p, base_point : t_point * t_point) : t_point =
      { x = p.x + base_point.x ; y = p.y + base_point.y}
  ;;

let draw_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
 draw_absolute_pt ( sum_point(p, base_point) , base_draw , dilat , col )
;;

clear_graph();;
draw_relative_pt ( {x=0 ; y=0},  {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;

(* ******************* Theo & Lucie ********************** *)
(** Fonction
     @author Theo & Lucie
 *)

let fill_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  fill_absolute_pt(sum_point(p, base_point), base_draw, dilat, col)
;;


(* ******************* Theo & Lucie ********************** *)
(** Fonction
     @author Theo & Lucie
 *)

let drawfill_relative_pt(p, base_point, base_draw, dilat, col : t_point * t_point * t_point * int * t_color) : unit =
  drawfill_absolute_pt(sum_point(p, base_point), base_draw, dilat, col)
;;


(* ******************* Theo ********************** *)
(** Fonction
     @author Theo
 *)

let rec draw_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , n : t_point list * t_point* t_point * int * t_color * int) : unit =
  if n > 1
  then(
    draw_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col);
    draw_pt_list_aux ( rem_fst ( pt_list ), base_pt, base_draw, dilat, col , n-1)
  )
  else
    draw_relative_pt (fst(pt_list), base_pt, base_draw, dilat, col)
;;

let draw_pt_list( pt_list, base_pt, base_draw, dilat, col : t_point list * t_point* t_point * int * t_color) : unit =
  draw_pt_list_aux ( pt_list, base_pt, base_draw, dilat, col , len(pt_list))
;; 

(* ******************* Theo / Xavier  ********************** *)
(** Fonction
     @author Theo, Xavier
 *)

let fill_pt_list(pt_list, base_pt, base_draw, dilat, col : t_point list * t_point * t_point * int * t_color) : unit =
  let list : t_point list ref = ref pt_list in
  while(!list <> [])
  do
    fill_relative_pt (fst(!list), base_pt, base_draw, dilat, col);
    list := rem_fst(!list);
  done;         
;;


clear_graph();;
fill_pt_list ( workspace (0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , red );;
clear_graph();;

(* ******************* Theo ********************** *)
(** Fonction
     @author Theo
 *)

let drawfill_pt_list(pt_list, base_pt, base_draw, dilat, col : t_point list * t_point * t_point * int * t_color) : unit =
  fill_pt_list(pt_list, base_pt, base_draw, dilat, col);
  draw_pt_list( pt_list, base_pt, base_draw, dilat, col)
;;


(* ****************** Leonard & Lucie ********************* *)
(** Fonction
     @author Leonard et Lucie
 *)

let draw_frame(base_draw, size_x, size_y, dilat : t_point * int * int * int) : unit =
  set_color(black);
  fill_rect(base_draw.x, base_draw.y, ((size_x + 1) * dilat), dilat - 1);
  fill_rect(base_draw.x, base_draw.y, dilat - 1 , ((size_y + 1) * dilat));
  fill_rect((size_x + 1) * dilat, base_draw.y, dilat - 1, ((size_y + 1) * dilat))
;;

(* ************************************************** *)
(** Fonction initialisation du graphe
     @author -none
 *)
let init ( u : unit ) : unit =
  clear_graph();
  draw_pt_list ( workspace (0 , 0 , []) , {x=0 ; y=0} , {x=50 ; y=50} , 20 , black );
  draw_frame({x=30; y =30}, 330, 500, 20);
;;


(* ******************* Xavier ********************** *)

(** Choisie alatoirement une couleur 
    @author Xavier
*)
let color_choice(t : t_color t_array) : t_color =
  t.value.(rand_int(0, t.len -1)) 
;;

color_choice(init_color());;

(* Initialise alatoirement un type t_cur_shape *)
let cur_shape_choice(shapes, mat_szx, mat_szy, color_arr : t_shape t_array * int * int * t_color t_array) : t_cur_shape =
  let rand_shape : int = rand_int(0,6)
  and rand_color : t_color = color_choice(color_arr)
  and point : t_point = {x = mat_szx/2; y = mat_szy} in
  ({base = ref point; shape = ref (rand_shape); color = ref rand_color})
;;

(* renvoie les points (x, y) d'une pice par le biais de son base point *)
let point(cur, p : t_cur_shape * t_point) : t_point =
  let base : t_point ref  = cur.base in  
  let base_x : int = !base.x
  and base_y : int = !base.y in
  {x = base_x + p.x; y = base_y + p.y}
;;

(* Test si la pice peut tre insrer *)  
let rec insert_aux(cur, shape, my_mat : t_cur_shape * t_point list * t_color matrix) : bool =
  if(shape = [])
  then true
  else
    let p : t_point = point(cur, fst(shape)) in
    if(my_mat.(p.y).(p.x) = 1)
    then false
    else insert_aux(cur, rem_fst(shape), my_mat)
;;

(* Test si la pice peut tre insrer *)  
let insert(cur, shape, param, my_mat : t_cur_shape * t_point list * t_param * t_color matrix) : bool =
  insert_aux(cur, shape, my_mat)
;;


(* ****************** theo *************************** *)

let valid_matrix_point ( p , param : t_point * t_param ) : bool =
 ( p.x >= 0 && p.x <= param_mat_szx(param)-1 )&& ( p.y >= 0 && p.y <= param_mat_szy(param)-1)
;;

valid_matrix_point ( {x= 15; y= 24} , init_param() ) ;;


(* *)
let rec is_free_move_aux ( p , shape , my_mat , param : t_point * t_point list * t_color matrix * t_param ) : bool =
  if shape = []
  then true
  else
    if valid_matrix_point ( fst(shape) , param ) = false
    then false
    else is_free_move_aux ( p , rem_fst(shape) , my_mat , param )
;;

let is_free_move ( p , shape , my_mat , param : t_point * t_point list * t_color matrix * t_param ) : bool =
  is_free_move_aux ( p , shape , my_mat , param )
;;


is_free_move ( {x= 0; y= 0} , shape_shape (init_sh011()) , play_mat (init_play()) , init_param() );;
 

let move_left ( pl : t_play ) : unit =
  let a : t_point ref =  cur_shape_base(play_cur_shape(pl)) in
  if is_free_move ( {x= 0; y= 0}, shape_shape (init_sh011()) , play_mat(init_play()), pl.par)
  then a:= {x= !a.x-1 ; y= !a.y }
  else failwith "erreur blabla"
;;


(* ----------------------------------------------- *)
(* ----------------------------------------------- *)
(*    Deplacements et controle des deplacements    *)
(* ----------------------------------------------- *)
(* ----------------------------------------------- *)

(* choix des deplacements suivant le caractere saisi *)
(*
let move(pl, dir : t_play * char) : bool = 
  (
  if dir = 't'
    then rotate_right(pl)
    else
      if dir = 'c'
      then rotate_left(pl)
      else
        if dir = 'd'
        then move_left(pl)
        else
          if dir = 'h'
          then move_right(pl)
          else () ;  
  (dir = 'v')
  )
;;
 



(* ----------------------------------- *)
(* ----------------------------------- *)
(*    Suppression des lignes pleines   *)
(* ----------------------------------- *)
(* ----------------------------------- *)


(* --------------------- *)
(* --------------------- *)
(*   Une etape de jeu    *)
(* --------------------- *)
(* --------------------- *)

let newstep(pl, new_t, t, dt : t_play * float ref * float * float) : bool = 
  let the_end : bool ref = ref (!new_t -. t > dt) and dec : bool ref = ref false in
  let dir : char ref = ref 'x' and notmove : bool ref = ref false in
    (
    while not(!the_end)
    do 
      if key_pressed()
      then dir := read_key()
      else () ;
      dec := move(pl, !dir) ;
      dir := 'x' ; 
      new_t := Sys.time() ;
      the_end := !dec || (!new_t -. t > dt) ;
    done ; 
    if !dec 
    then (move_at_bottom(pl) ; notmove := true)
    else notmove := not(move_down(pl)) ;
    if !notmove
    then the_end := final_newstep(pl)
    else the_end := false;
    !the_end ;
    )
;;

(* ------------------------ *)
(* ------------------------ *)
(*    Fonction principale   *)
(* ------------------------ *)
(* ------------------------ *)


let jeuCP2() : unit =
  let pl : t_play = init_play() in
  let t : float ref = ref (Sys.time()) and new_t : float ref = ref (Sys.time()) in
  let dt : float ref = ref (time_init(pl.par)) and t_acc : float ref = ref (Sys.time()) in
  let the_end : bool ref = ref false in
    while not(!the_end)
    do
      the_end := newstep(pl, new_t, !t, !dt) ; 
      if ((!new_t -. !t_acc) > time_extent(pl.par))
      then 
        (
        dt := !dt *. time_ratio(pl.par) ; 
        t_acc := !new_t
        ) 
      else () ;
      t := !new_t
    done
;;
 *)
